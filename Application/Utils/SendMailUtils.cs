﻿using Application.Commons;
using Domain.Entities;
using Domain.Enums;
using System.Net;
using System.Net.Mail;

namespace Application.Utils
{
    public static class SendMailUtils
    {
        public static async void SendMailConfirmationBookingToCustomerAndGuide(this Booking booking, EmailConfiguration emailConfiguration)
        {
            //SmtpClient smtpClient = new SmtpClient
            //{
            //    Port = 587,
            //    EnableSsl = true,
            //    Host = "smtp.gmail.com",
            //    UseDefaultCredentials = false,
            //    Credentials = new NetworkCredential(emailConfiguration.EmailForSend, emailConfiguration.AppPasswordConfiguration),
            //};
            string customerName = booking.CustomerID.HasValue ? booking.Customer.FullName : booking.AnonymousName;
            string phoneNumberCustomer = booking.CustomerID.HasValue ? booking.Customer.PhoneNumber : booking.AnonymousPhoneNumber;
            string email = booking.CustomerID.HasValue ? booking.Customer.Email : booking.AnonymousEmail;
            string amountFormatVND = booking.TotalAmount.ToString("N0");
            string tourDate = booking.BookingDate.ToString("MM/dd/yyyy");
            string paymentMethodVietNamese = booking.PaymentMethod == PaymentMethodEnum.Cash ? "Tiền mặt" : "Chuyển khoản";
            MailMessage messageCustomer = new MailMessage
            {
                Subject = "[Xác nhận/Confirmation] Đơn đặt tour của bạn đã được xác nhận - Your Booking Has Been Confirmed - TripMate",
                Body = $@"
Chào {customerName},

Cảm ơn bạn đã chọn TripMate làm đối tác của mình. Đơn đặt tour của bạn đã được xác nhận.

Thông tin đơn đặt tour của bạn:
- Ngày đặt: {booking.CreationDate}
- Ngày đi: {tourDate}
- Thời gian bắt đầu: {booking.StartTime}
- Tổng tiền: {amountFormatVND} VND
- Phương thức thanh toán: {paymentMethodVietNamese}

**Thông tin liên hệ của bạn:
- Tên: {customerName}
- Email: {email}
- Số điện thoại: {phoneNumberCustomer}

**Thông tin hướng dẫn viên:
- Tên: {booking.Guide.FullName}
- Email: {booking.Guide.Email}
- Số điện thoại: {booking.Guide.PhoneNumber}

Cảm ơn bạn đã sử dụng dịch vụ của chúng tôi. Nếu bạn có bất kỳ câu hỏi hoặc cần hỗ trợ thêm, xin vui lòng liên hệ với chúng tôi qua email tripmatefpt@gmail.com.

Lưu ý: Đây là email tự động, xin vui lòng không trả lời email này.

---

Dear {customerName},

Thank you for choosing TripMate as your partner. Your booking has been confirmed.

Your booking information:
- Booking Date: {booking.CreationDate}
- Tour Date: {tourDate}
- Start Time: {booking.StartTime}
- Total Amount: {amountFormatVND} VND
- Payment Method: {booking.PaymentMethod}

**Your Contact Information:
- Name: {customerName}
- Email: {email}
- Phone Number: {phoneNumberCustomer}

**Guide Information:
- Name: {booking.Guide.FullName}
- Email: {booking.Guide.Email}
- Phone Number: {booking.Guide.PhoneNumber}

Thank you for using our service. If you have any questions or need further assistance, please contact us via email at tripmatefpt@gmail.com.

Note: This is an automated email, please do not reply to this email.",
                From = new MailAddress(emailConfiguration.EmailForSend),
                To = { new MailAddress(email) },
            };
            MailMessage messageGuide = new MailMessage
            {
                Subject = "Thông báo về tour mới/Notification of New Tour Assignment - TripMate",
                Body = $@"
Chào {booking.Guide.FullName},

Chúng tôi rất vui thông báo rằng bạn đã được chỉ định làm hướng dẫn viên cho một tour mới.

Dưới đây là thông tin chi tiết về tour:
- Ngày đặt: {booking.CreationDate}
- Ngày đi: {tourDate}
- Thời gian bắt đầu: {booking.StartTime}
- Tổng tiền: {amountFormatVND} VND
- Phương thức thanh toán: {paymentMethodVietNamese}

**Thông tin khách hàng:
- Tên: {customerName}
- Email: {email}
- Số điện thoại: {phoneNumberCustomer}

Nếu bạn có bất kỳ câu hỏi hoặc cần hỗ trợ thêm, xin vui lòng liên hệ với chúng tôi qua email tripmatefpt@gmail.com.

Lưu ý: Đây là email tự động, xin vui lòng không trả lời email này.

---

Dear {booking.Guide.FullName},

We are pleased to inform you that you have been assigned as the tour guide for a new upcoming tour.

Here are the details of the tour:
- Booking Date: {booking.CreationDate}
- Tour Date: {tourDate}
- Start Time: {booking.StartTime}
- Total Amount: {amountFormatVND} VND
- Payment Method: {booking.PaymentMethod}

**Customer Information:
- Name: {customerName}
- Email: {email}
- Phone Number: {phoneNumberCustomer}

If you have any questions or need further assistance, please contact us via email at tripmatefpt@gmail.com.

Note: This is an automated email, please do not reply to this email.",
                From = new MailAddress(emailConfiguration.EmailForSend),
                To = { new MailAddress(booking.Guide.Email) },
            };
            await SendMailAsync(messageGuide, emailConfiguration);
            await SendMailAsync(messageCustomer, emailConfiguration);
        }
        public static async void SendMailNewBookingToAdmin(this Booking booking, EmailConfiguration emailConfiguration)
        {
            string emailTripMate = "tripmatefpt@gmail.com";
            string customerName = booking.CustomerID.HasValue ? booking.Customer.FullName : booking.AnonymousName;
            string phoneNumberCustomer = booking.CustomerID.HasValue ? booking.Customer.PhoneNumber : booking.AnonymousPhoneNumber;
            string email = booking.CustomerID.HasValue ? booking.Customer.Email : booking.AnonymousEmail;
            string amountFormatVND = booking.TotalAmount.ToString("N0");
            string tourDate = booking.BookingDate.ToString("MM/dd/yyyy");
            string paymentMethodVietNamese = booking.PaymentMethod == PaymentMethodEnum.Cash ? "Tiền mặt" : "Chuyển khoản";
            MailMessage message = new MailMessage
            {
                Subject = $"Thông báo về tour mới ({booking.CreationDate}) - TripMate",
                Body = $@"
Một đơn đặt tour mới vừa được thực hiện.

Dưới đây là thông tin chi tiết về đơn đặt tour:
- Ngày đặt: {booking.CreationDate}
- Ngày đi: {tourDate}
- Thời gian bắt đầu: {booking.StartTime}
- Tổng tiền: {amountFormatVND} VND
- Phương thức thanh toán: {paymentMethodVietNamese}

**Thông tin khách hàng:
- Tên: {customerName}
- Email: {email}
- Số điện thoại: {phoneNumberCustomer}

**Thông tin hướng dẫn viên:
- Tên: {booking.Guide.FullName}
- Email: {booking.Guide.Email}
- Số điện thoại: {booking.Guide.PhoneNumber}
- Tên ngân hàng: {booking.Guide.BankName}
- STK ngân hàng: {booking.Guide.BankNumber}

Vui lòng kiểm tra và xử lý đơn đặt tour này.

Lưu ý: Đây là email tự động, xin vui lòng không trả lời email này.
",
                From = new MailAddress(emailConfiguration.EmailForSend),
                To = { new MailAddress(emailTripMate)},
            };
            await SendMailAsync(message, emailConfiguration);
        }
        private static async Task SendMailAsync(MailMessage message, EmailConfiguration emailConfiguration)
        {
            SmtpClient smtpClient = new SmtpClient
            {
                Port = 587,
                EnableSsl = true,
                Host = "smtp.gmail.com",
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(emailConfiguration.EmailForSend, emailConfiguration.AppPasswordConfiguration),
            };
            await smtpClient.SendMailAsync(message);
        }
    }
}
