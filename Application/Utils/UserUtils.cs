﻿using Domain.Entities;

namespace Application.Utils
{
    public static class UserUtils
    {
       public static void UpdateTotalBookings(this User user)
       {
           user.TotalBookings++;
       }
    }
}
