﻿using Application.Commons;
using Domain.Entities;
using Domain.Enums;

namespace Application.Utils
{
    public static class BookingUtils
    {
        public static void InitTransactionCash(this Booking booking)
        {
            booking.Transactions = new List<Transaction>();
            var transactions = new Transaction();
            var platformFee = (int)(booking.TotalAmount * AppPricing.Revenue);
            transactions = new Transaction()
            {
                UserId = booking.GuideID,
                Amount = platformFee,
                From = "Guide",
                To = "Tripmate",
                TransactionType = TransactionTypeEnum.PlatformFee,
            };
            booking.Transactions.Add(transactions);
        }

        public static void InitTransactionBankTransfer(this Booking booking)
        {
            var guideFee = (int)(booking.TotalAmount * (1 - AppPricing.Revenue));

            booking.Transactions = new List<Transaction>
            {
                new Transaction
                {
                    UserId = booking.CustomerID,
                    Amount = booking.TotalAmount,
                    From = "User",
                    To = "Tripmate",
                    TransactionType = TransactionTypeEnum.TourPayment,
                    Status = StatusTransactionEnum.Completed
                },
                new Transaction
                {
                    UserId = booking.GuideID,
                    Amount = guideFee,
                    From = "Tripmate",
                    To = "Guide",
                    TransactionType = TransactionTypeEnum.GuideFee
                }
            };
        }
    }
}

