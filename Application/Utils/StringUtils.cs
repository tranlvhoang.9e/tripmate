﻿using System.Security.Cryptography;
using System.Text;

namespace Application.Utils
{
    public static class StringUtils
    {
        public static string HashPassword(this string password, string salt)
        {
            string passwordHash;
            SHA256 hash256 = SHA256.Create();
            string input = password + salt;
            byte[] inputBytes = Encoding.UTF8.GetBytes(input);
            byte[] hash = hash256.ComputeHash(inputBytes);
            IEnumerable<string> hex = hash.Select(x => x.ToString("x2"));
            passwordHash = string.Join("", hex);
            return passwordHash;
        }
    }
}
