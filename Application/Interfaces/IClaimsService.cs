﻿using Domain.Enums;

namespace Application.Interfaces
{
    public interface IClaimsService
    {
        public Guid GetCurrentUserId { get; }
        public RoleEnum? GetCurrentUserRole { get; }
        public string GetToken { get; set; }
    }
}
