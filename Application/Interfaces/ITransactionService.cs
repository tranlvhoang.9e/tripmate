﻿using Application.Commons;
using Application.ViewModels.TransactionViewModels;

namespace Application.Interfaces
{
    public interface ITransactionService
    {
        Task<ApiResponse<List<TransactionViewModel>>> GetAllTransactionAsync();
        Task<ApiResponse<List<TransactionViewModel>>> GetAllTransactionGuideAsync();
        Task<ApiResponse<string>> UpdateStatusTransactionAsync(string id, int status);
        Task<ApiResponse<string>> ConfirmPaymentTransactionGuideAsync(string id);
        Task<ApiResponse<Pagination<TransactionViewModel>>> FilterTransactionAsync(TransactionFilterModel transactionFilterModel);
        Task<ApiResponse<Pagination<TransactionViewModel>>> FilterTransactionGuideAsync(TransactionFilterModel transactionFilterModel);
        Task<ApiResponse<int>> GetTotalEarningAsync();
    }
}
