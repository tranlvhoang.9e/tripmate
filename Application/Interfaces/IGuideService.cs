﻿using Application.Commons;
using Application.ViewModels.GuideViewModels;
using Application.ViewModels.UserViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IGuideService
    {
        Task<ApiResponse<string>> BanGuideAsync(string id);
        Task<ApiResponse<Pagination<GuideViewModel>>> FilterGuideAsync(GuideFilterModel guideFilterModel);
        Task<ApiResponse<List<GuideViewModel>>> GetAllGuidesAsync();
        Task<ApiResponse<BankInformation>> GetBankInformationGuide(string id);
        Task<ApiResponse<GuideViewDetailModel>> GetCurrentGuideAsync();
        Task<ApiResponse<GuideViewDetailModel>> GetDetailGuidAsync(string id);
        Task<ApiResponse<Pagination<GuideViewForAdminModel>>> GetFilteredAdminGuides(GuideFilterModel guideFilterModel);
        Task<ApiResponse<Pagination<GuideViewModel>>> GetGuidesPaginationAsync(int pageIndex, int pageSize);
        Task<ApiResponse<List<GuideViewTop5Model>>> GetTop5GuideAsync();
        Task<ApiResponse<int>> GetTotalBookingGuideLoginAsync();
        Task<ApiResponse<int>> GetTotalHourBookingGuideLoginAsync();
        Task<ApiResponse<string>> UpdateGuideInformationAsync(UpdateGuideModel updateGuideModel);
        Task<ApiResponse<string>> UpdateStatusGuideAsync();
    }
}
