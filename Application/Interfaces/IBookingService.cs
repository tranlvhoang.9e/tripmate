﻿using Application.Commons;
using Application.ViewModels.BookingViewModels;
using Application.ViewModels.StatisticViewModels;
using System.Data;

namespace Application.Interfaces
{
    public interface IBookingService
    {
        Task<ApiResponse<BookingViewDetailModel>> CreateBookingCustomerAsync(BookingCustomerCreationModel objectBookingCustomer);
        Task<ApiResponse<BookingViewDetailModel>> CreateBookingGuestAsync(BookingGuestCreationModel objectBookingGuest);
        Task<ApiResponse<BookingViewDetailModel>> GetBookingDetailAsync(string id);
        Task<ApiResponse<List<BookingViewModel>>> GetAllBookingAsync();
        Task<ApiResponse<Pagination<BookingViewModel>>> GetBookingPaginationAsync(int pageNumber, int pageSize);
        Task<ApiResponse<List<BookingCustomerViewModel>>> GetAllBookingCustomerLoginAsync();
        Task<ApiResponse<Pagination<BookingCustomerViewModel>>> GetBookingCustomerLoginPaginationAsync(int pageNumber, int pageSize);
        Task<ApiResponse<List<BookingGuideViewModel>>> GetAllBookingGuideLoginAsync();
        Task<ApiResponse<Pagination<BookingGuideViewModel>>> GetBookingGuideLoginPaginationAsync(int pageNumber, int pageSize);
        Task<ApiResponse<string>> UpdateStatusBookingAsync(string id, int status);
        Task<ApiResponse<Pagination<BookingViewModel>>> GetBookingFilterAsync(BookingFilterModel bookingFilterModel);
        Task<ApiResponse<Pagination<BookingGuideViewModel>>> GetBookingGuideFilterAsync(BookingFilterModel bookingFilterModel);
        Task<ApiResponse<Pagination<BookingCustomerViewModel>>> GetBookingCustomerFilterAsync(BookingFilterModel bookingFilterModel);
        Task<ApiResponse<BookingViewDetailForAdminModel>> GetBookingDetailForAdminAsync(string id);
        Task<ApiResponse<int>> GetTotalBookingCompletedAsync();
        Task<ApiResponse<int>> GetTotalRevenueAsync();
        Task<ApiResponse<List<StatisticTourDurationViewModel>>> GetTourDurationStatisticsAsync();
    }
}
