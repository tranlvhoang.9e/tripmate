﻿using Application.Commons;
using Application.ViewModels.UserViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IUserService
    {
        Task<ApiResponse<string>> BanCustomerAsync(string id);
        Task<ApiResponse<string>> ChangePasswordAsync(ChangePasswordModel changePasswordModel);
        Task<ApiResponse<Pagination<CustomerViewModel>>> FiltelCustomerAsync(CustomerFilterModel customerFilterModel);
        Task<ApiResponse<UserViewModel>> GetCurrentUserAsync();
        Task<ApiResponse<List<CustomerViewTop5Model>>> GetTop5CustomerAsync();
        Task<ApiResponse<int>> GetTotalNewCustomerTodayAsync();
        Task<ApiResponse<AuthResponseModel>> LoginAsync(UserLoginModel loginObject);
        Task<ApiResponse<string>> LogoutAsync();
        Task<ApiResponse<string>> RegisterUserAsync(UserRegisterModel registerObject);

    }
}
