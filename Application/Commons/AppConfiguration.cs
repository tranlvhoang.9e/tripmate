﻿namespace Application.Commons
{
    public class AppConfiguration
    {
        public string DatabaseConnection { get; set; }
        public JWTSection JWTSection { get; set; }
        public EmailConfiguration EmailConfiguration { get; set; }
    }
}
