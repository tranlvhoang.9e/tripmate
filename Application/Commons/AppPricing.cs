﻿namespace Application.Commons
{
    public static class AppPricing
    {
        public const int PricePerHour = 75000;
        public const float Revenue = 0.3f;
    }
}
