﻿namespace Application.Commons
{
    public static class AppRole
    {
        public const string ADMIN = "Admin";
        public const string CUSTOMER = "Customer";
        public const string GUIDE = "Guide";
    }
}
