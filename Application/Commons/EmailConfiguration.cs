﻿namespace Application.Commons
{
    public class EmailConfiguration
    {
        public string EmailForSend { get; set; }
        public string AppPasswordConfiguration { get; set; }
    }
}
