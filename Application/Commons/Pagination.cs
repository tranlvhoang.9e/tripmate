﻿namespace Application.Commons
{
    public class Pagination<T>
    {
        public int TotalItemsCount { get; set; }
        public int PageSize { get; set; }
        public int TotalPagesCount
        {
            get
            {
                var temp = TotalItemsCount / PageSize;
                if (TotalItemsCount % PageSize == 0)
                {
                    return temp;
                }
                return temp + 1;
            }
        }
        public int PageIndex { get; set; }

        /// <summary>
        /// page index start from 1
        /// </summary>
        public bool Next => PageIndex < TotalPagesCount;
        public bool Previous => PageIndex > 1;
        public ICollection<T> Items { get; set; }
    }
}
