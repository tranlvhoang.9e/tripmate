﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.StatisticViewModels
{
    public class StatisticTourDurationViewModel
    {
        public int Duration { get; set; }
        public int Count { get; set; }
    }
}
    