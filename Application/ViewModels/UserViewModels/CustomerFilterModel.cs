﻿using Application.Enums;
using Domain.Enums;

namespace Application.ViewModels.UserViewModels
{
    public class CustomerFilterModel
    {
        public string? Search { get; set; }
        public StatusCustomerEnum? Status { get; set; }
        public CustomerSortEnum? SortBy { get; set; }
        public OrderSortEnum? SortOrder { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public int PageIndex { get; set; } = 1;
        public int PageSize { get; set; } = 10;
    }
}
