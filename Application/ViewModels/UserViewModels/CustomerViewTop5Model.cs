﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.UserViewModels
{
    public class CustomerViewTop5Model
    {
        public string Id { get; set; }
        public string ImageURL { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public ushort TotalBookings { get; set; }
    }
}
