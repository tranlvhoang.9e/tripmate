﻿using Application.Enums;
using Domain.Enums;

namespace Application.ViewModels.UserViewModels
{
    public class CustomerViewModel
    {
        public string Id { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Gender { get; set; }
        public string Status { get; set; }
        public DateTime RegisterDay { get; set; }
        public int TotalBooking { get; set; }
    }
}
