﻿using Application.Enums;
using Domain.Enums;

namespace Application.ViewModels.TransactionViewModels
{
    public class TransactionFilterModel
    {
        public string? Search { get; set; }
        public int? MinAmount { get; set; }
        public int? MaxAmount { get; set; }
        public TransactionTypeEnum? TransactionType { get; set; }
        public StatusTransactionEnum? Status { get; set; }
        public StatusBookingEnum? BookingStatus { get; set; }
        public TransactionSortEnum? SortBy { get; set; }
        public OrderSortEnum? SortOrder { get; set; }
        public DateTime? FromTime { get; set; }
        public DateTime? ToTime { get; set; }
        public int PageIndex { get; set; } = 1;
        public int PageSize { get; set; } = 10;
    }
}
