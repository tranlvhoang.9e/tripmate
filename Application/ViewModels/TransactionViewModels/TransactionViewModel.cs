﻿using Domain.Enums;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Application.ViewModels.TransactionViewModels
{
    public class TransactionViewModel
    {
        public string Id { get; set; }
        public int Amount { get; set; }
        public string Sender { get; set; }
        public string Receiver { get; set; }
        public string TransactionType { get; set; }
        public string Status { get; set; }
        public string UserId { get; set; }
        public string BookingId { get; set; }
        public string BookingStatus { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
