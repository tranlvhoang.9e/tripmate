﻿using Domain.Enums;

namespace Application.ViewModels.BookingViewModels
{
    public class BookingViewDetailModel
    {
        public string Id { get; set; }
        public UserBasicInformationModel UserBookingInformation { get; set; }
        public GuideBasicInformationModel GuideInformation { get; set; }
        public int PaymentMethod { get; set; }
        public DateTime BookingDate { get; set; }
        public string StartTime { get; set; }
        public ushort TotalHour { get; set; }
        public string Location { get; set; }
        public int Status { get; set; }
        public int TotalAmount { get; set; }
    }
}
