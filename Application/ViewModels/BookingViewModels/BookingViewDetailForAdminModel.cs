﻿using Application.ViewModels.TransactionViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.BookingViewModels
{
    public class BookingViewDetailForAdminModel
    {
        public UserBasicInformationModel UserBookingInformation { get; set; }
        public GuideBasicInformationModel GuideInformation { get; set; }
        public int PaymentMethod { get; set; }
        public DateTime BookingDate { get; set; }
        public string StartTime { get; set; }
        public ushort TotalHour { get; set; }
        public string Location { get; set; }
        public string Status { get; set; }
        public int TotalAmount { get; set; }
        public TransactionViewModel Transaction { get; set; }
    }
}
