﻿using Domain.Enums;

namespace Application.ViewModels.BookingViewModels
{
    public class BookingViewModel
    {
        public string Id { get; set; }
        public DateTime BookingDate { get; set; }
        public string StartTime { get; set; }
        public int TotalAmount { get; set; }
        public int PaymentMethod { get; set; }
        public int Status { get; set; }
        public DateTime CreationDate { get; set; }
        public string CustomerID { get; set; }
        public string CustomerName { get; set; }
        public string GuideID { get; set; }
        public string GuideName { get; set; }
    }
}
