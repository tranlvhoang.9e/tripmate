﻿using Domain.Enums;

namespace Application.ViewModels.BookingViewModels
{
    public class BookingGuestCreationModel
    {
        public string FullName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string GuideID { get; set; }
        public DateTime BookingDate { get; set; }
        public string StartTime { get; set; }
        public ushort TotalHour { get; set; }
        public string Location { get; set; }
        public int PaymentMethod { get; set; }
    }
}
