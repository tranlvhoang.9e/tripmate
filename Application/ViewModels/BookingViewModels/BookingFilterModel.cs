﻿using Application.Enums;
using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.BookingViewModels
{
    public class BookingFilterModel
    {
        public string? Search { get; set; }
        public int? MinAmount { get; set; }
        public int? MaxAmount { get; set; }
        public PaymentMethodEnum? PaymentMethod { get; set; }
        public StatusBookingEnum? Status { get; set; }
        public BookingSortEnum? SortBy { get; set; }
        public OrderSortEnum? SortOrder { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public int PageIndex { get; set; } = 1;
        public int PageSize { get; set; } = 10;
    }
}
