﻿using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.BookingViewModels
{
    public class BookingCustomerViewModel
    {
        public string Id { get; set; }
        public DateTime BookingDate { get; set; }
        public string StartTime { get; set; }
        public int TotalAmount { get; set; }
        public int PaymentMethod { get; set; }
        public int Status { get; set; }
        public string GuideID { get; set; }
        public string GuideName { get; set; }
    }
}
