﻿using Domain.Enums;

namespace Application.ViewModels.BookingViewModels
{
    public class BookingCustomerCreationModel
    {
        public string GuideID { get; set; }
        public DateTime BookingDate { get; set; }
        public string StartTime { get; set; }
        public ushort TotalHour { get; set; }
        public string Location { get; set; }
        public int PaymentMethod { get; set; }
    }
}
