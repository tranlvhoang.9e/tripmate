﻿namespace Application.ViewModels.BookingViewModels
{
    public class GuideBasicInformationModel
    { 
        public string Id { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Gender { get; set; }
        public string Languages { get; set; }
        public string BankName { get; set; }
        public string BankNumber { get; set; }
    }
}
