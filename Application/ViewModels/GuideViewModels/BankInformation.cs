﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.GuideViewModels
{
    public class BankInformation
    {
        public string BankName { get; set; }
        public string BankNumber { get; set; }
    }
}
