﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.GuideViewModels
{
    public class UpdateGuideModel
    {
        public string? Bio { get; set; }
        public string? Languages { get; set; }
    }
}
