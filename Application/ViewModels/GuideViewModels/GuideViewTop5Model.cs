﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.GuideViewModels
{
    public class GuideViewTop5Model
    {
        public string Id { get; set; }
        public string ImageURL { get; set; }
        public string FullName { get; set; }
        public ushort TotalBookings { get; set; }
    }
}
