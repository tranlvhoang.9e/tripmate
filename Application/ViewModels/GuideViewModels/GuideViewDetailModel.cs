﻿namespace Application.ViewModels.GuideViewModels
{
    public class GuideViewDetailModel
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string FullName { get; set; }
        public string Gender { get; set; }
        public string PhoneNumber { get; set; }
        public string? Bio { get; set; }
        public string? ImageURL { get; set; }
        public string? Languages { get; set; }
        public float? Rating { get; set; }
        public uint? TotalTour { get; set; }
        public string Status { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string? Address { get; set; }
        public string BankName { get; set; }
        public string BankNumber { get; set; }
    }
}
