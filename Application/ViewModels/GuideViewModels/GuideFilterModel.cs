﻿using Application.Enums;
using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.GuideViewModels
{
    public class GuideFilterModel
    {
        public string? Search { get; set; }
        public StatusUserEnum? Status { get; set; }
        public GuideSortEnum? SortBy { get; set; }
        public OrderSortEnum? SortOrder { get; set; }
        public int PageIndex { get; set; } = 1;
        public int PageSize { get; set; } = 10;
    }
}
