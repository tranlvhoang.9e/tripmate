﻿namespace Application.ViewModels.GuideViewModels
{
    public class GuideViewModel
    {
        public string Id { get; set; }
        public string FullName { get; set; }
        public string? ImageURL { get; set; }
        public string? Languages { get; set; }
        public float? Rating { get; set; }
        public uint? TotalTour { get; set; }
        public string Status { get; set; }
    }
}
