﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.GuideViewModels
{
    public class GuideViewForAdminModel
    {
        public string? Id { get; set; }
        public string? FullName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string? ImageURL { get; set; }
        public float? Rating { get; set; }
        public uint? Bookings { get; set; }
        public string? Status { get; set; }
    }
}
