﻿using Application.Commons;
using Application.Enums;
using Application.Interfaces;
using Application.ViewModels.GuideViewModels;
using Application.ViewModels.UserViewModels;
using AutoMapper;
using Domain.Entities;
using Domain.Enums;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class GuideService : IGuideService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ICurrentTime _currentTime;
        private readonly IClaimsService _claimsService;

        public GuideService(IUnitOfWork unitOfWork, IMapper mapper, ICurrentTime currentTime, IClaimsService claimsService)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _currentTime = currentTime;
            _claimsService = claimsService;
        }

        public async Task<ApiResponse<string>> BanGuideAsync(string id)
        {
            var response = new ApiResponse<string>();
            try
            {
                var guide = await _unitOfWork.GuideRepository.GetByIdAsync(Guid.Parse(id));
                guide.Status = StatusUserEnum.Banned;
                _unitOfWork.UserRepository.Update(guide);
                var isSuccess = await _unitOfWork.SaveChangeAsync() > 0;
                if (isSuccess)
                {
                    response.Message = "Ban guide successful";
                }
                else
                {
                    throw new Exception("Ban guide fail, please try again");
                }
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public async Task<ApiResponse<Pagination<GuideViewModel>>> FilterGuideAsync(GuideFilterModel guideFilterModel)
        {
            var response = new ApiResponse<Pagination<GuideViewModel>>();
            try
            {
                var filter = (Expression<Func<User, bool>>)(e =>
                e.Role.Equals(RoleEnum.Guide)
                &&
                (
                    guideFilterModel.Search == null
                    || e.Id.ToString().Contains(guideFilterModel.Search)
                    || e.FullName.Contains(guideFilterModel.Search)
                    || e.Languages.Contains(guideFilterModel.Search)
                )
                && (!guideFilterModel.Status.HasValue || e.Status == guideFilterModel.Status));

                Func<IQueryable<User>, IOrderedQueryable<User>>? orderBy = null;
                if (guideFilterModel.SortBy.HasValue && guideFilterModel.SortOrder.HasValue)
                {
                    switch (guideFilterModel.SortBy.Value)
                    {
                        case GuideSortEnum.TotalTour:
                            orderBy = guideFilterModel.SortOrder == OrderSortEnum.Desc
                                ? query => query.OrderByDescending(p => p.GuideBookings.Count)
                                : query => query.OrderBy(p => p.GuideBookings.Count);
                            break;
                        case GuideSortEnum.FullName:
                            orderBy = guideFilterModel.SortOrder == OrderSortEnum.Desc ?
                                query => query.OrderByDescending(p => p.FullName) :
                                query => query.OrderBy(p => p.FullName);
                            break;
                        case GuideSortEnum.Rating:
                            orderBy = guideFilterModel.SortOrder == OrderSortEnum.Desc ?
                                query => query.OrderByDescending(p => p.Rating) :
                                query => query.OrderBy(p => p.Rating);
                            break;

                    }
                }
                var guides = await _unitOfWork.UserRepository.Get
                (
                    includeProperties: "GuideBookings",
                    filter: filter,
                    orderBy: orderBy,
                    pageIndex: guideFilterModel.PageIndex,
                    pageSize: guideFilterModel.PageSize
                );
                var result = _mapper.Map<Pagination<GuideViewModel>>(guides);
                if (guides.TotalItemsCount == 0)
                {
                    response.Message = "Not found any guide";
                }
                else
                {
                    response.Message = "Filter successful";
                }
                response.Data = result;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public async Task<ApiResponse<List<GuideViewModel>>> GetAllGuidesAsync()
        {
            var response = new ApiResponse<List<GuideViewModel>>();
            try
            {
                var guides = await _unitOfWork.GuideRepository.GetAllAsync();
                var result = _mapper.Map<List<GuideViewModel>>(guides);
                response.Data = result;
                response.Message = $"Have {result.Count} guides.";
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public async Task<ApiResponse<BankInformation>> GetBankInformationGuide(string id)
        {
            var response = new ApiResponse<BankInformation>();
            try
            {
                var guide = await _unitOfWork.GuideRepository.GetByIdAsync(Guid.Parse(id));
                var result = _mapper.Map<BankInformation>(guide);
                response.Data = result;
                response.Message = $"Get bank information successfully.";
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public async Task<ApiResponse<GuideViewDetailModel>> GetCurrentGuideAsync()
        {
            var response = new ApiResponse<GuideViewDetailModel>();
            try
            {
                var guide = await _unitOfWork.GuideRepository.GetByIdAsync(_claimsService.GetCurrentUserId);
                var result = _mapper.Map<GuideViewDetailModel>(guide);
                response.Data = result;
                response.Message = $"Get information successfully.";
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public async Task<ApiResponse<GuideViewDetailModel>> GetDetailGuidAsync(string id)
        {
            var response = new ApiResponse<GuideViewDetailModel>();
            try
            {
                var guide = await _unitOfWork.GuideRepository.GetByIdAsync(Guid.Parse(id));
                var result = _mapper.Map<GuideViewDetailModel>(guide);
                response.Data = result;
                response.Message = $"Get guide details successfully.";
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public async Task<ApiResponse<Pagination<GuideViewForAdminModel>>> GetFilteredAdminGuides(GuideFilterModel guideFilterModel)
        {
            var response = new ApiResponse<Pagination<GuideViewForAdminModel>>();
            try
            {
                var filter = (Expression<Func<User, bool>>)(e =>
                e.Role.Equals(RoleEnum.Guide)
                &&
                (
                    guideFilterModel.Search == null
                    || e.Id.ToString().Contains(guideFilterModel.Search)
                    || e.Email.Contains(guideFilterModel.Search)
                    || e.FullName.Contains(guideFilterModel.Search)
                    || e.PhoneNumber.Contains(guideFilterModel.Search)
                )
                && (!guideFilterModel.Status.HasValue || e.Status == guideFilterModel.Status));

                Func<IQueryable<User>, IOrderedQueryable<User>>? orderBy = null;
                if (guideFilterModel.SortBy.HasValue && guideFilterModel.SortOrder.HasValue)
                {
                    switch (guideFilterModel.SortBy.Value)
                    {
                        case GuideSortEnum.TotalTour:
                            orderBy = guideFilterModel.SortOrder == OrderSortEnum.Desc
                                ? query => query.OrderByDescending(p => p.GuideBookings.Count)
                                : query => query.OrderBy(p => p.GuideBookings.Count);
                            break;
                        case GuideSortEnum.FullName:
                            orderBy = guideFilterModel.SortOrder == OrderSortEnum.Desc ?
                                query => query.OrderByDescending(p => p.FullName) :
                                query => query.OrderBy(p => p.FullName);
                            break;
                        case GuideSortEnum.Rating:
                            orderBy = guideFilterModel.SortOrder == OrderSortEnum.Desc ?
                                query => query.OrderByDescending(p => p.Rating) :
                                query => query.OrderBy(p => p.Rating);
                            break;

                    }
                }
                var guides = await _unitOfWork.UserRepository.Get
                (
                    includeProperties: "GuideBookings",
                    filter: filter,
                    orderBy: orderBy,
                    pageIndex: guideFilterModel.PageIndex,
                    pageSize: guideFilterModel.PageSize
                );
                var result = _mapper.Map<Pagination<GuideViewForAdminModel>>(guides);
                if (guides.TotalItemsCount == 0)
                {
                    response.Message = "Not found any guide";
                }
                else
                {
                    response.Message = "Filter successful";
                }
                response.Data = result;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public async Task<ApiResponse<Pagination<GuideViewModel>>> GetGuidesPaginationAsync(int pageNumber, int pageSize)
        {
            var response = new ApiResponse<Pagination<GuideViewModel>>();
            try
            {
                var guides = await _unitOfWork.GuideRepository.ToPagination(pageNumber - 1, pageSize);
                var result = _mapper.Map<Pagination<GuideViewModel>>(guides);
                response.Data = result;
                response.Message = $"Get pagination of guides successfully.";
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public async Task<ApiResponse<List<GuideViewTop5Model>>> GetTop5GuideAsync()
        {
            var response = new ApiResponse<List<GuideViewTop5Model>>();
            try
            {
                var guides = await _unitOfWork.GuideRepository.GetTop5GuideByTotalBookings();
                var result = _mapper.Map<List<GuideViewTop5Model>>(guides);
                response.Data = result;
                response.Message = $"Get top 5 guides successfully.";
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public async Task<ApiResponse<int>> GetTotalBookingGuideLoginAsync()
        {
            var response = new ApiResponse<int>();
            try
            {
                var result = await _unitOfWork.GuideRepository.GetTotalBookingGuideLoginAsync(_claimsService.GetCurrentUserId);
                response.Data = result;
                response.Message = $"Get totalbooking guides successfully.";
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public async Task<ApiResponse<int>> GetTotalHourBookingGuideLoginAsync()
        {
            var response = new ApiResponse<int>();
            try
            {
                var result = await _unitOfWork.GuideRepository.GetTotalHourBookingCurrentGuide(_claimsService.GetCurrentUserId);
                response.Data = result;
                response.Message = $"Get totalhour for all booking completed of guides successfully.";
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public async Task<ApiResponse<string>> UpdateGuideInformationAsync(UpdateGuideModel updateGuideModel)
        {
            var response = new ApiResponse<string>();
            try
            {
                var guide = await _unitOfWork.GuideRepository.GetByIdAsync(_claimsService.GetCurrentUserId);
                if (string.IsNullOrEmpty(updateGuideModel.Bio) is false)
                {
                    guide.Bio = updateGuideModel.Bio;
                }
                if(string.IsNullOrEmpty(updateGuideModel.Languages) is false)
                {
                    guide.Languages = updateGuideModel.Languages;
                }
                _unitOfWork.UserRepository.Update(guide);
                var isSuccess = await _unitOfWork.SaveChangeAsync() > 0;
                if (!isSuccess)
                {
                    throw new Exception("Update guide fail, please try again.");
                }
                response.Message = $"Update guide successful.";
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public async Task<ApiResponse<string>> UpdateStatusGuideAsync()
        {
            var response = new ApiResponse<string>();
            try
            {
                var guide = await _unitOfWork.GuideRepository.GetByIdAsync(_claimsService.GetCurrentUserId);
                guide.Status = guide.Status == StatusUserEnum.Active ? StatusUserEnum.Inactive : StatusUserEnum.Active;
                _unitOfWork.UserRepository.Update(guide);
                var isSuccess = await _unitOfWork.SaveChangeAsync() > 0;
                if (!isSuccess)
                {
                    throw new Exception("Update status of guide fail, please try again.");
                }
                response.Message = $"Update status of guide successful.";
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }
    }
}
