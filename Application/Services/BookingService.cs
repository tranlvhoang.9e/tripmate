﻿using Application.Commons;
using Application.Enums;
using Application.Interfaces;
using Application.Utils;
using Application.ViewModels.BookingViewModels;
using Application.ViewModels.StatisticViewModels;
using AutoMapper;
using Domain.Entities;
using Domain.Enums;
using FluentValidation;
using System.Linq.Expressions;
using ValidationResult = FluentValidation.Results.ValidationResult;

namespace Application.Services
{
    public class BookingService : IBookingService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ICurrentTime _currentTime;
        private readonly IClaimsService _claimsService;
        private readonly AppConfiguration _configuration;
        private readonly IValidator<BookingCustomerCreationModel> _validatorCustomerBooking;
        private readonly IValidator<BookingGuestCreationModel> _validatorGuestBooking;

        public BookingService(IUnitOfWork unitOfWork, IMapper mapper, ICurrentTime currentTime, IClaimsService claimsService, IValidator<BookingCustomerCreationModel> validatorCustomerBooking, IValidator<BookingGuestCreationModel> validatorGuestBooking, AppConfiguration configuration)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _currentTime = currentTime;
            _claimsService = claimsService;
            _validatorCustomerBooking = validatorCustomerBooking;
            _validatorGuestBooking = validatorGuestBooking;
            _configuration = configuration;
        }

        public async Task<ApiResponse<BookingViewDetailModel>> CreateBookingCustomerAsync(BookingCustomerCreationModel objectBookingCustomer)
        {
            var response = new ApiResponse<BookingViewDetailModel>();
            ValidationResult validationResult = await _validatorCustomerBooking.ValidateAsync(objectBookingCustomer);
            try
            {
                if (!validationResult.IsValid)
                {
                    throw new Exception(string.Join(", ", validationResult.Errors.Select(error => error.ErrorMessage)));
                }
                var newBooking = _mapper.Map<Booking>(objectBookingCustomer);
                var guide = await _unitOfWork.GuideRepository.GetByIdAsync(newBooking.GuideID);
                if (guide.Status != StatusUserEnum.Active)
                {
                    throw new Exception("Booking can only be made with active guides");
                }
                if (await _unitOfWork.BookingRepository.CheckExistedBookingTimeGuide(newBooking.GuideID, newBooking.BookingDate, newBooking.StartTime, newBooking.TotalHour))
                {
                    throw new Exception("The guide is already booked for another tour during this time frame.");
                }
                newBooking.CustomerID = _claimsService.GetCurrentUserId;
                newBooking.TotalAmount = AppPricing.PricePerHour * newBooking.TotalHour;
                newBooking.CreationDate = _currentTime.GetCurrentTime();
                //if (newBooking.PaymentMethod == PaymentMethodEnum.Cash)
                //{
                //    newBooking.InitTransactionCash();
                //}   
                await _unitOfWork.BookingRepository.AddAsync(newBooking);
                var isSuccess = await _unitOfWork.SaveChangeAsync() > 0;
                if (isSuccess)
                {
                    newBooking.Guide = guide;
                    newBooking.Customer = await _unitOfWork.UserRepository.GetByIdAsync((Guid)newBooking.CustomerID);
                    newBooking.SendMailNewBookingToAdmin(_configuration.EmailConfiguration);
                    response.Data = _mapper.Map<BookingViewDetailModel>(newBooking);
                    response.Message = "Create booking for customer success";
                }
                else
                {
                    throw new Exception("Create booking fail! please try again!");
                }
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public async Task<ApiResponse<BookingViewDetailModel>> CreateBookingGuestAsync(BookingGuestCreationModel objectBookingGuest)
        {
            var response = new ApiResponse<BookingViewDetailModel>();
            ValidationResult validationResult = await _validatorGuestBooking.ValidateAsync(objectBookingGuest);
            try
            {
                if (!validationResult.IsValid)
                {
                    throw new Exception(string.Join(", ", validationResult.Errors.Select(error => error.ErrorMessage)));
                }
                var newBooking = _mapper.Map<Booking>(objectBookingGuest);
                var guide = await _unitOfWork.GuideRepository.GetByIdAsync(newBooking.GuideID);
                if (guide.Status != StatusUserEnum.Active)
                {
                    throw new Exception("Booking can only be made with active guides");
                }
                if (await _unitOfWork.BookingRepository.CheckExistedBookingTimeGuide(newBooking.GuideID, newBooking.BookingDate, newBooking.StartTime, newBooking.TotalHour))
                {
                    throw new Exception("The guide is already booked for another tour during this time frame.");
                }
                newBooking.TotalAmount = AppPricing.PricePerHour * newBooking.TotalHour;
                newBooking.CreationDate = _currentTime.GetCurrentTime();
                //if (newBooking.PaymentMethod == PaymentMethodEnum.Cash)
                //{
                //    newBooking.InitTransactionCash();
                //}
                await _unitOfWork.BookingRepository.AddAsync(newBooking);
                var isSuccess = await _unitOfWork.SaveChangeAsync() > 0;
                if (isSuccess)
                {
                    newBooking.Guide = await _unitOfWork.UserRepository.GetByIdAsync((Guid)newBooking.GuideID);
                    response.Data = _mapper.Map<BookingViewDetailModel>(newBooking);
                    response.Message = "Create booking for guest success";
                }
                else
                {
                    throw new Exception("Create booking fail! please try again!");
                }
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public async Task<ApiResponse<List<BookingViewModel>>> GetAllBookingAsync()
        {
            var response = new ApiResponse<List<BookingViewModel>>();
            try
            {
                var bookings = await _unitOfWork.BookingRepository.GetAllWithRelated();
                var result = _mapper.Map<List<BookingViewModel>>(bookings);
                response.Data = result;
                response.Message = $"Have {result.Count} bookings.";
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public async Task<ApiResponse<List<BookingCustomerViewModel>>> GetAllBookingCustomerLoginAsync()
        {
            var response = new ApiResponse<List<BookingCustomerViewModel>>();
            try
            {
                var bookings = await _unitOfWork.BookingRepository.GetAllBookingByCustomerId(_claimsService.GetCurrentUserId);
                var result = _mapper.Map<List<BookingCustomerViewModel>>(bookings);
                response.Data = result;
                response.Message = $"Have {result.Count} bookings.";
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public async Task<ApiResponse<List<BookingGuideViewModel>>> GetAllBookingGuideLoginAsync()
        {
            var response = new ApiResponse<List<BookingGuideViewModel>>();
            try
            {
                var bookings = await _unitOfWork.BookingRepository.GetAllBookingByGuideId(_claimsService.GetCurrentUserId);
                var result = _mapper.Map<List<BookingGuideViewModel>>(bookings);
                response.Data = result;
                response.Message = $"Have {result.Count} bookings.";
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public async Task<ApiResponse<Pagination<BookingCustomerViewModel>>> GetBookingCustomerFilterAsync(BookingFilterModel bookingFilterModel)
        {
            var response = new ApiResponse<Pagination<BookingCustomerViewModel>>();
            try
            {
                var filter = (Expression<Func<Booking, bool>>)(e =>
                (
                    bookingFilterModel.Search == null
                    || e.Id.ToString().Contains(bookingFilterModel.Search)
                    || e.Guide.FullName.Contains(bookingFilterModel.Search)
                )
                && e.CustomerID.Equals(_claimsService.GetCurrentUserId)
                && (!bookingFilterModel.MinAmount.HasValue || e.TotalAmount >= bookingFilterModel.MinAmount)
                && (!bookingFilterModel.MaxAmount.HasValue || e.TotalAmount <= bookingFilterModel.MaxAmount)
                && (!bookingFilterModel.FromDate.HasValue || e.BookingDate >= bookingFilterModel.FromDate)
                && (!bookingFilterModel.ToDate.HasValue || e.BookingDate <= bookingFilterModel.ToDate)
                && (!bookingFilterModel.PaymentMethod.HasValue || e.PaymentMethod == bookingFilterModel.PaymentMethod)
                && (!bookingFilterModel.Status.HasValue || e.Status == bookingFilterModel.Status));

                Func<IQueryable<Booking>, IOrderedQueryable<Booking>>? orderBy = null;
                if (bookingFilterModel.SortBy.HasValue && bookingFilterModel.SortOrder.HasValue)
                {
                    switch (bookingFilterModel.SortBy.Value)
                    {
                        case BookingSortEnum.TotalAmount:
                            orderBy = bookingFilterModel.SortOrder == OrderSortEnum.Desc
                                ? query => query.OrderByDescending(p => p.TotalAmount)
                                : query => query.OrderBy(p => p.TotalAmount);
                            break;
                        case BookingSortEnum.BookingDate:
                            orderBy = bookingFilterModel.SortOrder == OrderSortEnum.Desc ?
                                query => query.OrderByDescending(p => p.BookingDate) :
                                query => query.OrderBy(p => p.BookingDate);
                            break;
                    }
                }
                var bookings = await _unitOfWork.BookingRepository.Get
                (
                    includeProperties: "Guide",
                    filter: filter,
                    orderBy: orderBy,
                    pageIndex: bookingFilterModel.PageIndex,
                    pageSize: bookingFilterModel.PageSize
                );
                var result = _mapper.Map<Pagination<BookingCustomerViewModel>>(bookings);
                if (bookings.TotalItemsCount == 0)
                {
                    response.Message = "Not found any transaction";
                }
                else
                {
                    response.Message = "Filter successful";
                }
                response.Data = result;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public async Task<ApiResponse<Pagination<BookingCustomerViewModel>>> GetBookingCustomerLoginPaginationAsync(int pageNumber, int pageSize)
        {
            var response = new ApiResponse<Pagination<BookingCustomerViewModel>>();
            try
            {
                var bookings = await _unitOfWork.BookingRepository.GetBookingCustomerLoginPagination(_claimsService.GetCurrentUserId, pageNumber - 1, pageSize);
                var result = _mapper.Map<Pagination<BookingCustomerViewModel>>(bookings);
                response.Data = result;
                response.Message = $"Get pagination of bookings successfully.";
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public async Task<ApiResponse<BookingViewDetailModel>> GetBookingDetailAsync(string id)
        {
            var response = new ApiResponse<BookingViewDetailModel>();
            try
            {
                var guide = await _unitOfWork.BookingRepository.GetDetailBookingById(Guid.Parse(id));
                var result = _mapper.Map<BookingViewDetailModel>(guide);
                response.Data = result;
                response.Message = $"Get booking details successfully.";
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public async Task<ApiResponse<BookingViewDetailForAdminModel>> GetBookingDetailForAdminAsync(string id)
        {
            var response = new ApiResponse<BookingViewDetailForAdminModel>();
            try
            {
                var guide = await _unitOfWork.BookingRepository.GetDetailBookingForAdminById(Guid.Parse(id));
                var result = _mapper.Map<BookingViewDetailForAdminModel>(guide);
                response.Data = result;
                response.Message = $"Get booking details successfully.";
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public async Task<ApiResponse<Pagination<BookingViewModel>>> GetBookingFilterAsync(BookingFilterModel bookingFilterModel)
        {
            var response = new ApiResponse<Pagination<BookingViewModel>>();
            try
            {
                var filter = (Expression<Func<Booking, bool>>)(e =>
                (
                    bookingFilterModel.Search == null
                    || e.Id.ToString().Contains(bookingFilterModel.Search)
                    || e.Guide.FullName.Contains(bookingFilterModel.Search)
                    || (e.CustomerID.HasValue ? e.Customer.FullName.Contains(bookingFilterModel.Search) : e.AnonymousName.Contains(bookingFilterModel.Search))
                )
                && (!bookingFilterModel.MinAmount.HasValue || e.TotalAmount >= bookingFilterModel.MinAmount)
                && (!bookingFilterModel.MaxAmount.HasValue || e.TotalAmount <= bookingFilterModel.MaxAmount)
                && (!bookingFilterModel.FromDate.HasValue || e.BookingDate >= bookingFilterModel.FromDate)
                && (!bookingFilterModel.ToDate.HasValue || e.BookingDate <= bookingFilterModel.ToDate)
                && (!bookingFilterModel.PaymentMethod.HasValue || e.PaymentMethod == bookingFilterModel.PaymentMethod)
                && (!bookingFilterModel.Status.HasValue || e.Status == bookingFilterModel.Status));

                Func<IQueryable<Booking>, IOrderedQueryable<Booking>>? orderBy = null;
                if (bookingFilterModel.SortBy.HasValue && bookingFilterModel.SortOrder.HasValue)
                {
                    switch (bookingFilterModel.SortBy.Value)
                    {
                        case BookingSortEnum.TotalAmount:
                            orderBy = bookingFilterModel.SortOrder == OrderSortEnum.Desc
                                ? query => query.OrderByDescending(p => p.TotalAmount)
                                : query => query.OrderBy(p => p.TotalAmount);
                            break;
                        case BookingSortEnum.BookingDate:
                            orderBy = bookingFilterModel.SortOrder == OrderSortEnum.Desc ?
                                query => query.OrderByDescending(p => p.BookingDate) :
                                query => query.OrderBy(p => p.BookingDate);
                            break;
                    }
                }
                var bookings = await _unitOfWork.BookingRepository.Get
                (
                includeProperties: "Customer,Guide",
                filter: filter,
                        orderBy: orderBy,
                        pageIndex: bookingFilterModel.PageIndex,
                        pageSize: bookingFilterModel.PageSize
                    );
                var result = _mapper.Map<Pagination<BookingViewModel>>(bookings);
                if (bookings.TotalItemsCount == 0)
                {
                    response.Message = "Not found any transaction";
                }
                else
                {
                    response.Message = "Filter successful";
                }
                response.Data = result;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public async Task<ApiResponse<Pagination<BookingGuideViewModel>>> GetBookingGuideFilterAsync(BookingFilterModel bookingFilterModel)
        {
            var response = new ApiResponse<Pagination<BookingGuideViewModel>>();
            try
            {
                var filter = (Expression<Func<Booking, bool>>)(e =>
                (
                    bookingFilterModel.Search == null
                    || e.Id.ToString().Contains(bookingFilterModel.Search)
                    || (e.CustomerID.HasValue ? e.Customer.FullName.Contains(bookingFilterModel.Search) : e.AnonymousName.Contains(bookingFilterModel.Search))
                )
                && e.GuideID.Equals(_claimsService.GetCurrentUserId)
                && (!bookingFilterModel.MinAmount.HasValue || e.TotalAmount >= bookingFilterModel.MinAmount)
                && (!bookingFilterModel.MaxAmount.HasValue || e.TotalAmount <= bookingFilterModel.MaxAmount)
                && (!bookingFilterModel.FromDate.HasValue || e.BookingDate >= bookingFilterModel.FromDate)
                && (!bookingFilterModel.ToDate.HasValue || e.BookingDate <= bookingFilterModel.ToDate)
                && (!bookingFilterModel.PaymentMethod.HasValue || e.PaymentMethod == bookingFilterModel.PaymentMethod)
                && (!bookingFilterModel.Status.HasValue || e.Status == bookingFilterModel.Status));

                Func<IQueryable<Booking>, IOrderedQueryable<Booking>>? orderBy = null;
                if (bookingFilterModel.SortBy.HasValue && bookingFilterModel.SortOrder.HasValue)
                {
                    switch (bookingFilterModel.SortBy.Value)
                    {
                        case BookingSortEnum.TotalAmount:
                            orderBy = bookingFilterModel.SortOrder == OrderSortEnum.Desc
                                ? query => query.OrderByDescending(p => p.TotalAmount)
                                : query => query.OrderBy(p => p.TotalAmount);
                            break;
                        case BookingSortEnum.BookingDate:
                            orderBy = bookingFilterModel.SortOrder == OrderSortEnum.Desc ?
                                query => query.OrderByDescending(p => p.BookingDate) :
                                query => query.OrderBy(p => p.BookingDate);
                            break;
                    }
                }
                var bookings = await _unitOfWork.BookingRepository.Get
                (
                    includeProperties: "Customer",
                    filter: filter,
                    orderBy: orderBy,
                    pageIndex: bookingFilterModel.PageIndex,
                    pageSize: bookingFilterModel.PageSize
                );
                var result = _mapper.Map<Pagination<BookingGuideViewModel>>(bookings);
                if (bookings.TotalItemsCount == 0)
                {
                    response.Message = "Not found any transaction";
                }
                else
                {
                    response.Message = "Filter successful";
                }
                response.Data = result;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public async Task<ApiResponse<Pagination<BookingGuideViewModel>>> GetBookingGuideLoginPaginationAsync(int pageNumber, int pageSize)
        {
            var response = new ApiResponse<Pagination<BookingGuideViewModel>>();
            try
            {
                var bookings = await _unitOfWork.BookingRepository.GetBookingGuideLoginPagination(_claimsService.GetCurrentUserId, pageNumber - 1, pageSize);
                var result = _mapper.Map<Pagination<BookingGuideViewModel>>(bookings);
                response.Data = result;
                response.Message = $"Get pagination of bookings successfully.";
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public async Task<ApiResponse<Pagination<BookingViewModel>>> GetBookingPaginationAsync(int pageNumber, int pageSize)
        {
            var response = new ApiResponse<Pagination<BookingViewModel>>();
            try
            {
                var bookings = await _unitOfWork.BookingRepository.ToPaginationWithRelated(pageNumber - 1, pageSize);
                var result = _mapper.Map<Pagination<BookingViewModel>>(bookings);
                response.Data = result;
                response.Message = $"Get pagination of bookings successfully.";
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public async Task<ApiResponse<int>> GetTotalBookingCompletedAsync()
        {
            var response = new ApiResponse<int>();
            try
            {
                var totalBookingsCompleted = await _unitOfWork.BookingRepository.GetTotalBookingsCompleted();
                response.Data = totalBookingsCompleted;
                response.Message = $"Get total booking completed successfully.";
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public async Task<ApiResponse<int>> GetTotalRevenueAsync()
        {
            var response = new ApiResponse<int>();
            try
            {
                var totalRevenue = await _unitOfWork.BookingRepository.GetTotalRevenueAsync();
                response.Data = totalRevenue;
                response.Message = $"Get total revenue successfully.";
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public async Task<ApiResponse<List<StatisticTourDurationViewModel>>> GetTourDurationStatisticsAsync()
        {
            var response = new ApiResponse<List<StatisticTourDurationViewModel>>();
            try
            {
                var bookings = await _unitOfWork.BookingRepository.GetAllBookingCompleted();
                var result = bookings.GroupBy(x => x.TotalHour)
                    .Select(x => new StatisticTourDurationViewModel
                    {
                        Duration = x.Key,
                        Count = x.Count()
                    })
                    .OrderBy(x => x.Duration)
                    .ToList();
                response.Data = result;
                response.Message = $"Get duration statistics successfully.";
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public async Task<ApiResponse<string>> UpdateStatusBookingAsync(string id, int status)
        {
            var response = new ApiResponse<string>();
            try
            {
                if (!Enum.IsDefined(typeof(StatusBookingEnum), status))
                {
                    throw new Exception("Invalid status");
                }
                var newStatus = (StatusBookingEnum)status;
                var role = _claimsService.GetCurrentUserRole;
                Booking booking = new Booking();
                bool isSuccess = false;
                switch (role)
                {
                    case RoleEnum.Admin:
                        booking = await _unitOfWork.BookingRepository.GetByIdAsync(Guid.Parse(id));
                        if (booking.Status == StatusBookingEnum.Completed)
                        {
                            throw new Exception($"Can not change status anymore because the status of booking is completed");
                        }
                        if (booking.Status >= newStatus)
                        {
                            throw new Exception("Can not update status " + newStatus.ToString() + " again");
                        }
                        if (booking.PaymentMethod == PaymentMethodEnum.BankTransfer && newStatus == StatusBookingEnum.Confirmed)
                        {
                            booking.InitTransactionBankTransfer();
                            await _unitOfWork.TransactionRepository.AddRangeAsync(booking.Transactions.ToList());
                        }
                        else if (booking.PaymentMethod == PaymentMethodEnum.Cash && newStatus == StatusBookingEnum.Confirmed)
                        {
                            booking.InitTransactionCash();
                            await _unitOfWork.TransactionRepository.AddAsync(booking.Transactions.First());
                        }
                        booking.Status = newStatus;
                        //_unitOfWork.BookingRepository.Update(booking);
                        //isSuccess = await _unitOfWork.SaveChangeAsync() > 0;
                        break;
                    case RoleEnum.Guide:
                        booking = await _unitOfWork.BookingRepository.GetByIdAsync(Guid.Parse(id));
                        if (booking.Status == StatusBookingEnum.Completed)
                        {
                            throw new Exception($"Can not change status anymore because the status of booking is completed");
                        }
                        if (booking.Status >= newStatus)
                        {
                            throw new Exception("Can not update status " + newStatus.ToString() + " again");
                        }
                        if (booking.PaymentMethod == PaymentMethodEnum.BankTransfer && booking.Status == StatusBookingEnum.Pending)
                        {
                            throw new Exception("Guide not update status when status of booking is pending and the payment method is Bank Transfer");
                        }
                        if (booking.PaymentMethod == PaymentMethodEnum.Cash && newStatus == StatusBookingEnum.Confirmed)
                        {
                            booking.InitTransactionCash();
                            await _unitOfWork.TransactionRepository.AddAsync(booking.Transactions.First());
                        }
                        booking.Status = newStatus;
                        //_unitOfWork.BookingRepository.Update(booking);
                        //isSuccess = await _unitOfWork.SaveChangeAsync() > 0;
                        break;
                    case RoleEnum.Customer:
                        if (newStatus != StatusBookingEnum.Cancelled)
                        {
                            throw new Exception("Customer can change status is Cancelled only.");
                        }
                        booking = await _unitOfWork.BookingRepository.GetByIdAsync(Guid.Parse(id));
                        if (booking.Status == StatusBookingEnum.Cancelled)
                        {
                            throw new Exception("Can not update status " + newStatus.ToString() + " again");
                        }
                        booking.Status = newStatus;
                        //_unitOfWork.BookingRepository.Update(booking);
                        //isSuccess = await _unitOfWork.SaveChangeAsync() > 0;
                        break;
                    default:
                        throw new Exception("Invalid authorize");
                };
                if (booking.Status == StatusBookingEnum.Completed)
                {
                    //if(_currentTime.GetCurrentTime().CompareTo(booking.BookingDate) < 0)
                    //{
                    //    throw new Exception("Can not change status to completed before the tour date");
                    //}
                    var guide = await _unitOfWork.GuideRepository.GetByIdAsync(booking.GuideID);
                    guide.TotalBookings++;
                    _unitOfWork.UserRepository.Update(guide);
                    if (booking.CustomerID != null)
                    {
                        var customer = await _unitOfWork.UserRepository.GetByIdAsync((Guid)booking.CustomerID);
                        customer.TotalBookings++;
                        _unitOfWork.UserRepository.Update(customer);
                    }
                }
                _unitOfWork.BookingRepository.Update(booking);
                isSuccess = await _unitOfWork.SaveChangeAsync() > 0;
                if (isSuccess is true)
                {
                    response.Message = $"Update status of booking successfully.";
                    if(booking.Status == StatusBookingEnum.Confirmed)
                    {
                        await _unitOfWork.BookingRepository.LoadGuideAndCustomerInformation(booking);
                        booking.SendMailConfirmationBookingToCustomerAndGuide(_configuration.EmailConfiguration);
                    }
                }
                else
                {
                    response.Message = $"Update status of booking fail. Please try again";
                }
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }
    }
}
