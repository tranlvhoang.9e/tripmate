﻿using Application;
using Application.Commons;
using Application.Interfaces;
using Application.ViewModels.UserViewModels;
using AutoMapper;
using Domain.Entities;
using Domain.Enums;
using FluentValidation;
using System.Security.Cryptography;
using ValidationResult = FluentValidation.Results.ValidationResult;
using Application.Utils;
using System.Linq.Expressions;
using Application.Enums;
using Microsoft.AspNetCore.Identity;
using System.Data.Common;
using Application.ViewModels.GuideViewModels;

namespace Infrastructures.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ICurrentTime _currentTime;
        private readonly AppConfiguration _configuration;
        private readonly IValidator<UserRegisterModel> _validatorRegister;
        private readonly IValidator<ChangePasswordModel> _validatorChangePassword;
        private readonly IClaimsService _claimsService;

        public UserService(IUnitOfWork unitOfWork, IMapper mapper, ICurrentTime currentTime, AppConfiguration configuration, IValidator<UserRegisterModel> validatorRegister, IClaimsService claimsService, IValidator<ChangePasswordModel> validatorChangePassword)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _currentTime = currentTime;
            _configuration = configuration;
            _validatorRegister = validatorRegister;
            _claimsService = claimsService;
            _validatorChangePassword = validatorChangePassword;
        }

        public async Task<ApiResponse<string>> BanCustomerAsync(string id)
        {
            var response = new ApiResponse<string>();
            try
            {
                var customer = await _unitOfWork.UserRepository.GetByIdAsync(Guid.Parse(id));
                if (customer.Role != RoleEnum.Customer)
                {
                    throw new Exception("Something wrong, please try again");
                }
                customer.Status = StatusUserEnum.Banned;
                _unitOfWork.UserRepository.Update(customer);
                var isSuccess = await _unitOfWork.SaveChangeAsync() > 0;
                if (isSuccess)
                {
                    response.Message = "Ban customer successful";
                }
                else
                {
                    throw new Exception("Ban customer fail, please try again");
                }
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public async Task<ApiResponse<string>> ChangePasswordAsync(ChangePasswordModel changePasswordModel)
        {
            var response = new ApiResponse<string>();
            ValidationResult validationResult = await _validatorChangePassword.ValidateAsync(changePasswordModel);
            try
            {
                if (!validationResult.IsValid)
                {
                    throw new Exception(string.Join(", ", validationResult.Errors.Select(error => error.ErrorMessage)));
                }
                var userID = _claimsService.GetCurrentUserId;
                var user = await _unitOfWork.UserRepository.GetByIdAsync(userID);
                if (user.PasswordHash != changePasswordModel.OldPassword.HashPassword(user.Salt))
                {
                    throw new Exception("The old password is not correct!");
                }
                user.PasswordHash = changePasswordModel.NewPassword.HashPassword(user.Salt);
                _unitOfWork.UserRepository.Update(user);
                var isSuccess = await _unitOfWork.SaveChangeAsync() > 0;
                if (!isSuccess)
                {
                    throw new Exception("Change password fail, please try again");
                }
                response.Message = "Change password successful!";
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public async Task<ApiResponse<Pagination<CustomerViewModel>>> FiltelCustomerAsync(CustomerFilterModel customerFilterModel)
        {
            var response = new ApiResponse<Pagination<CustomerViewModel>>();
            try
            {
                StatusUserEnum? customerStatus = customerFilterModel.Status.HasValue ? (StatusUserEnum)customerFilterModel.Status : null;
                var filter = (Expression<Func<User, bool>>)(e =>
                e.Role.Equals(RoleEnum.Customer)
                &&
                (
                    customerFilterModel.Search == null
                    || e.Id.ToString().Contains(customerFilterModel.Search)
                    || e.FullName.Contains(customerFilterModel.Search)
                )
                && (!customerFilterModel.FromDate.HasValue || e.CreationDate >= customerFilterModel.FromDate)
                && (!customerFilterModel.ToDate.HasValue || e.CreationDate <= customerFilterModel.ToDate)
                && (!customerFilterModel.Status.HasValue || e.Status == customerStatus));

                Func<IQueryable<User>, IOrderedQueryable<User>>? orderBy = null;
                if (customerFilterModel.SortBy.HasValue && customerFilterModel.SortOrder.HasValue)
                {
                    switch (customerFilterModel.SortBy.Value)
                    {
                        case CustomerSortEnum.TotalBooing:
                            orderBy = customerFilterModel.SortOrder == OrderSortEnum.Desc
                                ? query => query.OrderByDescending(p => p.CustomerBookings.Count)
                                : query => query.OrderBy(p => p.CustomerBookings.Count);
                            break;
                        case CustomerSortEnum.RegisterDay:
                            orderBy = customerFilterModel.SortOrder == OrderSortEnum.Desc ?
                                query => query.OrderByDescending(p => p.CreationDate) :
                                query => query.OrderBy(p => p.CreationDate);
                            break;
                        case CustomerSortEnum.FullName:
                            orderBy = customerFilterModel.SortOrder == OrderSortEnum.Desc ?
                                query => query.OrderByDescending(p => p.FullName) :
                                query => query.OrderBy(p => p.FullName);
                            break;

                    }
                }
                var customers = await _unitOfWork.UserRepository.Get
                (
                    includeProperties: "CustomerBookings",
                    filter: filter,
                    orderBy: orderBy,
                    pageIndex: customerFilterModel.PageIndex,
                    pageSize: customerFilterModel.PageSize
                );
                var result = _mapper.Map<Pagination<CustomerViewModel>>(customers);
                if (customers.TotalItemsCount == 0)
                {
                    response.Message = "Not found any customer";
                }
                else
                {
                    response.Message = "Filter successful";
                }
                response.Data = result;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public async Task<ApiResponse<UserViewModel>> GetCurrentUserAsync()
        {
            var response = new ApiResponse<UserViewModel>();
            try
            {
                var guide = await _unitOfWork.UserRepository.GetByIdAsync(_claimsService.GetCurrentUserId);
                var result = _mapper.Map<UserViewModel>(guide);
                response.Data = result;
                response.Message = $"Get information successfully.";
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public async Task<ApiResponse<List<CustomerViewTop5Model>>> GetTop5CustomerAsync()
        {
            var response = new ApiResponse<List<CustomerViewTop5Model>>();
            try
            {
                var customers = await _unitOfWork.UserRepository.GetTop5GuideByTotalBookings();
                var result = _mapper.Map<List<CustomerViewTop5Model>>(customers);
                response.Data = result;
                response.Message = $"Get top 5 customers successfully.";
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public async Task<ApiResponse<int>> GetTotalNewCustomerTodayAsync()
        {
            var response = new ApiResponse<int>();
            try
            {
                var totalCustomersNewToday = await _unitOfWork.UserRepository.GetTotalNewCustomerToday();
                response.Data = totalCustomersNewToday;
                response.Message = $"Get total new customer today successfully.";
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public async Task<ApiResponse<AuthResponseModel>> LoginAsync(UserLoginModel loginObject)
        {
            var response = new ApiResponse<AuthResponseModel>();
            try
            {
                User? user = await _unitOfWork.UserRepository.GetUserByEmailAndPassword(loginObject.Email, loginObject.Password);
                if (user.Status is StatusUserEnum.Banned)
                {
                    throw new Exception("Your account has been banned. You cannot log in.");
                }
                var token = user.GenerateJsonWebToken(_configuration.JWTSection.SecretKey, _currentTime.GetCurrentTime(), _configuration.JWTSection.Issuer, _configuration.JWTSection.Audience, _configuration.JWTSection.ExpiresInMinutes);
                var result = _mapper.Map<AuthResponseModel>(user);
                result.Token = token;
                response.Data = result;
                response.Message = "Login successful.";
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public async Task<ApiResponse<string>> LogoutAsync()
        {
            var response = new ApiResponse<string>();
            try
            {
                var token = _claimsService.GetToken;
                if (string.IsNullOrEmpty(token))
                {
                    throw new Exception("Token is missing");
                }
                TokenBlacklist tokenBlack = new TokenBlacklist
                {
                    Token = token,
                    BlacklistedAt = _currentTime.GetCurrentTime()
                };
                await _unitOfWork.TokenBlackListRepository.AddAsync(tokenBlack);
                var isSuccess = await _unitOfWork.SaveChangeAsync() > 0;
                if (isSuccess)
                {
                    response.Message = "Logout Successful!";
                }
                else
                {
                    throw new Exception("Logout fail");
                }
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public async Task<ApiResponse<string>> RegisterUserAsync(UserRegisterModel registerObject)
        {
            ValidationResult validationResult = await _validatorRegister.ValidateAsync(registerObject);
            var response = new ApiResponse<string>();
            try
            {
                if (!validationResult.IsValid)
                {
                    throw new Exception(string.Join(", ", validationResult.Errors.Select(error => error.ErrorMessage)));
                }
                var isExisted = await _unitOfWork.UserRepository.CheckEmailNameExisted(registerObject.Email);
                if (isExisted)
                {
                    throw new Exception("Email existed please try again");
                }
                isExisted = await _unitOfWork.UserRepository.CheckPhoneNumberExisted(registerObject.PhoneNumber);
                if (isExisted)
                {
                    throw new Exception("Phone number existed please try again");
                }
                var newCustomer = _mapper.Map<User>(registerObject);
                newCustomer.Role = RoleEnum.Customer;

                //create salt
                byte[] randomBytes = RandomNumberGenerator.GetBytes(15);
                IEnumerable<string> saltHex = randomBytes.Select(x => x.ToString("x2"));
                string salt = string.Join("", saltHex);
                newCustomer.Salt = salt;
                newCustomer.PasswordHash = registerObject.Password.HashPassword(salt);
                newCustomer.CreationDate = _currentTime.GetCurrentTime();

                await _unitOfWork.UserRepository.AddAsync(newCustomer);
                var isSuccess = await _unitOfWork.SaveChangeAsync() > 0;
                if (isSuccess)
                {
                    response.Message = "Resigter success";
                }
                else
                {
                    throw new Exception("Resigter fail! Please try again");
                }
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        //    public async Task<string> LoginAsync(UserLoginDTO userObject)
        //    {
        //        var user = await _unitOfWork.UserRepository.GetUserByUserNameAndPasswordHash(userObject.UserName, userObject.Password.Hash());
        //        return user.GenerateJsonWebToken(_configuration.JWTSecretKey, _currentTime.GetCurrentTime());
        //    }

        //    public async Task RegisterAsync(UserLoginDTO userObject)
        //    {
        //        // check username exited
        //        var isExited = await _unitOfWork.UserRepository.CheckUserNameExited(userObject.UserName);

        //        if (isExited)
        //        {
        //            throw new Exception("Username exited please try again");
        //        }

        //        var newUser = new User
        //        {
        //            UserName = userObject.UserName,
        //            PasswordHash = userObject.Password.Hash()
        //        };

        //        await _unitOfWork.UserRepository.AddAsync(newUser);
        //        await _unitOfWork.SaveChangeAsync();
        //    }
        //}
    }
}
