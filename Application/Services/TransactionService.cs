﻿using Application.Commons;
using Application.Enums;
using Application.Interfaces;
using Application.ViewModels.TransactionViewModels;
using AutoMapper;
using Domain.Entities;
using Domain.Enums;
using System.Linq.Expressions;

namespace Application.Services
{
    public class TransactionService : ITransactionService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ICurrentTime _currentTime;
        private readonly IClaimsService _claimsService;


        public TransactionService(IUnitOfWork unitOfWork, IMapper mapper, ICurrentTime currentTime, IClaimsService claimsService)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _currentTime = currentTime;
            _claimsService = claimsService;
        }

        public async Task<ApiResponse<string>> ConfirmPaymentTransactionGuideAsync(string id)
        {
            var response = new ApiResponse<string>();
            try
            {
                var transaction = await _unitOfWork.TransactionRepository.GetByIdAsync(Guid.Parse(id));
                if (transaction.UserId != _claimsService.GetCurrentUserId)
                {
                    throw new Exception("You do not have permission to do this action");
                }
                if (transaction.TransactionType != TransactionTypeEnum.PlatformFee)
                {
                    throw new Exception("You do not have permission to do this action");
                }
                if (transaction.Status == StatusTransactionEnum.PendingConfirmed)
                {
                    throw new Exception("The transaction have paid");
                }
                transaction.Status = StatusTransactionEnum.PendingConfirmed;
                _unitOfWork.TransactionRepository.Update(transaction);
                if (await _unitOfWork.SaveChangeAsync() > 0)
                {
                    response.Message = $"Confirm payment transaction successful.";
                }
                else
                {
                    response.Message = $"Confirm payment transaction fail.";
                }
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public async Task<ApiResponse<Pagination<TransactionViewModel>>> FilterTransactionAsync(TransactionFilterModel transactionFilterModel)
        {
            var response = new ApiResponse<Pagination<TransactionViewModel>>();
            try
            {
                var filter = (Expression<Func<Transaction, bool>>)(e =>
                (
                    transactionFilterModel.Search == null
                    || e.Id.ToString().Contains(transactionFilterModel.Search)
                    || e.User.FullName.Contains(transactionFilterModel.Search)
                    || e.From.Contains(transactionFilterModel.Search)
                    || e.To.Contains(transactionFilterModel.Search)
                )
                && (!transactionFilterModel.MinAmount.HasValue || e.Amount >= transactionFilterModel.MinAmount)
                && (!transactionFilterModel.MaxAmount.HasValue || e.Amount <= transactionFilterModel.MinAmount)
                && (!transactionFilterModel.FromTime.HasValue || e.CreationDate >= transactionFilterModel.FromTime)
                && (!transactionFilterModel.ToTime.HasValue || e.CreationDate <= transactionFilterModel.ToTime)
                && (!transactionFilterModel.TransactionType.HasValue || e.TransactionType == transactionFilterModel.TransactionType)
                && (!transactionFilterModel.Status.HasValue || e.Status == transactionFilterModel.Status)
                && (!transactionFilterModel.BookingStatus.HasValue || e.Booking.Status == transactionFilterModel.BookingStatus));

                Func<IQueryable<Transaction>, IOrderedQueryable<Transaction>>? orderBy = null;
                if (transactionFilterModel.SortBy.HasValue && transactionFilterModel.SortOrder.HasValue)
                {
                    switch (transactionFilterModel.SortBy.Value)
                    {
                        case TransactionSortEnum.Amount:
                            orderBy = transactionFilterModel.SortOrder == OrderSortEnum.Desc
                                ? query => query.OrderByDescending(p => p.Amount)
                                : query => query.OrderBy(p => p.Amount);
                            break;
                        case TransactionSortEnum.CreationDate:
                            orderBy = transactionFilterModel.SortOrder == OrderSortEnum.Desc ?
                                query => query.OrderByDescending(p => p.CreationDate) :
                                query => query.OrderBy(p => p.CreationDate);
                            break;
                    }
                }
                var transactions = await _unitOfWork.TransactionRepository.Get
                    (
                        includeProperties: "User,Booking",
                        filter: filter,
                        orderBy: orderBy,
                        pageIndex: transactionFilterModel.PageIndex,
                        pageSize: transactionFilterModel.PageSize
                    );
                var result = _mapper.Map<Pagination<TransactionViewModel>>(transactions);
                if (transactions.TotalItemsCount == 0)
                {
                    response.Message = "Not found any transaction";
                }
                else
                {
                    response.Message = "Filter successful";
                }
                response.Data = result;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public async Task<ApiResponse<Pagination<TransactionViewModel>>> FilterTransactionGuideAsync(TransactionFilterModel transactionFilterModel)
        {
            var response = new ApiResponse<Pagination<TransactionViewModel>>();
            try
            {
                var filter = (Expression<Func<Transaction, bool>>)(e =>
                (
                    transactionFilterModel.Search == null
                    || e.Id.ToString().Contains(transactionFilterModel.Search)
                    || e.User.FullName.Contains(transactionFilterModel.Search)
                    || e.From.Contains(transactionFilterModel.Search)
                    || e.To.Contains(transactionFilterModel.Search)
                )
                && e.UserId.Equals(_claimsService.GetCurrentUserId)
                && (!transactionFilterModel.MinAmount.HasValue || e.Amount >= transactionFilterModel.MinAmount)
                && (!transactionFilterModel.MaxAmount.HasValue || e.Amount <= transactionFilterModel.MinAmount)
                && (!transactionFilterModel.FromTime.HasValue || e.CreationDate >= transactionFilterModel.FromTime)
                && (!transactionFilterModel.ToTime.HasValue || e.CreationDate <= transactionFilterModel.ToTime)
                && (!transactionFilterModel.TransactionType.HasValue || e.TransactionType == transactionFilterModel.TransactionType)
                && (!transactionFilterModel.Status.HasValue || e.Status == transactionFilterModel.Status)
                && (!transactionFilterModel.BookingStatus.HasValue || e.Booking.Status == transactionFilterModel.BookingStatus));

                Func<IQueryable<Transaction>, IOrderedQueryable<Transaction>>? orderBy = null;
                if (transactionFilterModel.SortBy.HasValue && transactionFilterModel.SortOrder.HasValue)
                {
                    switch (transactionFilterModel.SortBy.Value)
                    {
                        case TransactionSortEnum.Amount:
                            orderBy = transactionFilterModel.SortOrder == OrderSortEnum.Desc
                                ? query => query.OrderByDescending(p => p.Amount)
                                : query => query.OrderBy(p => p.Amount);
                            break;
                        case TransactionSortEnum.CreationDate:
                            orderBy = transactionFilterModel.SortOrder == OrderSortEnum.Desc ?
                                query => query.OrderByDescending(p => p.CreationDate) :
                                query => query.OrderBy(p => p.CreationDate);
                            break;
                    }
                }
                var transactions = await _unitOfWork.TransactionRepository.Get
                    (
                        includeProperties: "User,Booking",
                        filter: filter,
                        orderBy: orderBy,
                        pageIndex: transactionFilterModel.PageIndex,
                        pageSize: transactionFilterModel.PageSize
                    );
                var result = _mapper.Map<Pagination<TransactionViewModel>>(transactions);
                if (transactions.TotalItemsCount == 0)
                {
                    response.Message = "Not found any transaction";
                }
                else
                {
                    response.Message = "Filter successful";
                }
                response.Data = result;
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public async Task<ApiResponse<List<TransactionViewModel>>> GetAllTransactionAsync()
        {
            var response = new ApiResponse<List<TransactionViewModel>>();
            try
            {
                var transactions = await _unitOfWork.TransactionRepository.GetAllWithRelated();
                var result = _mapper.Map<List<TransactionViewModel>>(transactions);
                response.Data = result;
                response.Message = $"Have {result.Count} transactions.";
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public async Task<ApiResponse<List<TransactionViewModel>>> GetAllTransactionGuideAsync()
        {
            var response = new ApiResponse<List<TransactionViewModel>>();
            try
            {
                var transactions = await _unitOfWork.TransactionRepository.GetAllTransactionOfGuideWithRelated(_claimsService.GetCurrentUserId);
                var result = _mapper.Map<List<TransactionViewModel>>(transactions);
                response.Data = result;
                response.Message = $"Have {result.Count} transactions.";
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public async Task<ApiResponse<int>> GetTotalEarningAsync()
        {
            var response = new ApiResponse<int>();
            try
            {
                var totalRevenue = await _unitOfWork.TransactionRepository.GetTotalEarningAsync();
                response.Data = totalRevenue;
                response.Message = $"Get total earning successfully.";
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public async Task<ApiResponse<string>> UpdateStatusTransactionAsync(string id, int status)
        {
            var response = new ApiResponse<string>();
            try
            {
                if (!Enum.IsDefined(typeof(StatusTransactionEnum), status))
                {
                    throw new Exception("Invalid status");
                }
                var newStatus = (StatusTransactionEnum)status;
                if (newStatus == StatusTransactionEnum.PendingConfirmed)
                {
                    throw new Exception("Only guide can update this state!");
                }
                var transaction = await _unitOfWork.TransactionRepository.GetByIdAsync(Guid.Parse(id));
                if (transaction.Status >= newStatus)
                {
                    throw new Exception("Can not update status " + newStatus.ToString() + " again");
                }
                transaction.Status = newStatus;
                _unitOfWork.TransactionRepository.Update(transaction);
                if (await _unitOfWork.SaveChangeAsync() > 0)
                {
                    response.Message = $"Update status transaction successful.";
                }
                else
                {
                    response.Message = $"Update status transaction fail.";
                }
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.Message = ex.Message;
            }
            return response;
        }
    }
}
