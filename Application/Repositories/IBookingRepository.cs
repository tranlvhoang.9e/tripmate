﻿using Application.Commons;
using Application.ViewModels.StatisticViewModels;
using Domain.Entities;

namespace Application.Repositories
{
    public interface IBookingRepository : IGenericRepository<Booking>
    {
        Task<bool> CheckExistedBookingTimeGuide(Guid guideId, DateTime date, string startTime, ushort totalHour);
        Task<Booking> GetDetailBookingById(Guid id);
        Task<Booking> GetDetailBookingForAdminById(Guid id);
        Task<List<Booking>> GetAllBookingByCustomerId(Guid id);
        Task<List<Booking>> GetAllBookingByGuideId(Guid id);
        Task<Pagination<Booking>> GetBookingCustomerLoginPagination(Guid customerId, int pageIndex, int pageSize);
        Task<Pagination<Booking>> GetBookingGuideLoginPagination(Guid guideId, int pageIndex, int pageSize);
        Task<List<Booking>> GetAllWithRelated();
        Task<Pagination<Booking>> ToPaginationWithRelated(int pageIndex, int pageSize);
        Task<int> GetTotalBookingsCompleted();
        Task<int> GetTotalRevenueAsync();
        Task<List<Booking>> GetAllBookingCompleted();
        Task LoadGuideAndCustomerInformation(Booking booking);
    }
}
