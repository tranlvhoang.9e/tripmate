﻿using Domain.Entities;

namespace Application.Repositories
{
    public interface ITransactionRepository : IGenericRepository<Transaction>
    {
        Task<List<Transaction>> GetAllTransactionOfGuideWithRelated(Guid guideId);
        Task<List<Transaction>> GetAllWithRelated();
        Task<int> GetTotalEarningAsync();
    }
}
