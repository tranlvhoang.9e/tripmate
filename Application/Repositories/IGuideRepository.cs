﻿using Application.Commons;
using Domain.Entities;

namespace Application.Repositories
{
    public interface IGuideRepository : IGenericRepository<User>
    {
        new Task<List<User>> GetAllAsync();
        new Task<Pagination<User>> ToPagination(int pageNumber = 0, int pageSize = 10);
        new Task<User> GetByIdAsync(Guid id);
        Task<bool> CheckExistedGuideById(Guid id);
        Task<List<User>> GetTop5GuideByTotalBookings();
        Task<int> GetTotalBookingGuideLoginAsync(Guid guideId);
        Task<int> GetTotalHourBookingCurrentGuide(Guid guideId);
    }
}
