﻿using Domain.Entities;

namespace Application.Repositories
{
    public interface ITokenBlackListRepository
    {
        Task AddAsync(TokenBlacklist token);
        Task<bool> IsTokenBlacklisted(string token);
    }
}
