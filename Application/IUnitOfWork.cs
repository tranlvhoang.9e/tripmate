﻿using Application.Repositories;

namespace Application
{
    public interface IUnitOfWork
    {
        public IUserRepository UserRepository { get; }
        public ITransactionRepository TransactionRepository { get; }
        public IReviewRepository ReviewRepository { get; }
        public IBookingRepository BookingRepository { get; }
        public IGuideRepository GuideRepository { get; }
        public ITokenBlackListRepository TokenBlackListRepository { get; }
        public Task<int> SaveChangeAsync();
    }
}
