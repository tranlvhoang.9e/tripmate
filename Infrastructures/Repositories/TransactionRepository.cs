﻿using Application.Interfaces;
using Application.Repositories;
using Domain.Entities;
using Domain.Enums;
using Microsoft.EntityFrameworkCore;

namespace Infrastructures.Repositories
{
    public class TransactionRepository : GenericRepository<Transaction>, ITransactionRepository
    {
        private readonly AppDbContext _dbContext;

        public TransactionRepository(AppDbContext dbContext,
            ICurrentTime timeService,
            IClaimsService claimsService)
            : base(dbContext,
                  timeService,
                  claimsService)
        {
            _dbContext = dbContext;
        }

        public Task<List<Transaction>> GetAllTransactionOfGuideWithRelated(Guid guideId) => _dbContext.Transactions
                                                                            .Where(x => x.UserId == guideId)
                                                                            .Include(x => x.User)
                                                                            .Include(x => x.Booking)
                                                                            .AsNoTracking()
                                                                            .ToListAsync();


        public Task<List<Transaction>> GetAllWithRelated() => _dbContext.Transactions
                                                                            .Include(x => x.User)
                                                                            .Include(x => x.Booking)
                                                                            .AsNoTracking()
                                                                            .ToListAsync();

        public async Task<int> GetTotalEarningAsync()
        {
            var totalEarning = await _dbSet
                        .Where(x => x.Booking.Status == StatusBookingEnum.Completed)
                        .GroupBy(x => true) // Grouping by a constant value since we want to aggregate over all rows
                        .Select(g => new
                        {
                            PlatformFee = g.Where(x => x.TransactionType == TransactionTypeEnum.PlatformFee && x.Status == StatusTransactionEnum.Completed).Sum(x => x.Amount),
                            TourPayment = g.Where(x => x.TransactionType == TransactionTypeEnum.TourPayment && x.Status == StatusTransactionEnum.Completed).Sum(x => x.Amount),
                            GuideFee = g.Where(x => x.TransactionType == TransactionTypeEnum.GuideFee && x.Status != StatusTransactionEnum.Refund).Sum(x => x.Amount)
                        })
                        .Select(t => t.TourPayment + t.PlatformFee - t.GuideFee)
                        .FirstOrDefaultAsync();

            return totalEarning;
        }
    }
}
