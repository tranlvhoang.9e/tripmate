﻿using Application.Commons;
using Application.Interfaces;
using Application.Repositories;
using Application.ViewModels.StatisticViewModels;
using Domain.Entities;
using Domain.Enums;
using Microsoft.EntityFrameworkCore;

namespace Infrastructures.Repositories
{
    public class BookingRepository : GenericRepository<Booking>, IBookingRepository
    {
        private readonly AppDbContext _dbContext;

        public BookingRepository(AppDbContext dbContext,
            ICurrentTime timeService,
            IClaimsService claimsService)
            : base(dbContext,
                  timeService,
                  claimsService)
        {
            _dbContext = dbContext;
        }

        public async Task<bool> CheckExistedBookingTimeGuide(Guid guideId, DateTime date, string startTime, ushort totalHour)
        {
            var isExisted = false;
            TimeSpan start = TimeSpan.Parse(startTime);
            TimeSpan duration = TimeSpan.FromHours(totalHour);
            DateTime newTourStart = date.Date + start;
            DateTime newTourEnd = newTourStart + duration;
            var today = DateTime.Today;
            var bookings = await _dbContext.Bookings
                                    .Where(x => x.GuideID == guideId &&
                                              x.BookingDate.Date >= today &&
                                              x.Status != StatusBookingEnum.Cancelled)
                                    .ToListAsync();
            isExisted = bookings.Any(x => newTourStart < x.BookingDate.Date + TimeSpan.Parse(x.StartTime) + TimeSpan.FromHours(x.TotalHour) &&
                                    newTourEnd > x.BookingDate.Date + TimeSpan.Parse(x.StartTime));

            return isExisted;
        }

        public Task<List<Booking>> GetAllBookingByCustomerId(Guid customerId) => _dbContext.Bookings
                                                                    .Where(x => x.CustomerID == customerId)
                                                                    .Include(x => x.Customer)
                                                                    .Include(x => x.Guide)
                                                                    .ToListAsync();

        public Task<List<Booking>> GetAllBookingByGuideId(Guid guideId) => _dbContext.Bookings
                                                                    .Where(x => x.GuideID == guideId)
                                                                    .Include(x => x.Customer)
                                                                    .Include(x => x.Guide)
                                                                    .ToListAsync();

        public Task<List<Booking>> GetAllWithRelated() => _dbContext.Bookings
                                                                    .Include(x => x.Customer)
                                                                    .Include(x => x.Guide)
                                                                    .ToListAsync();

        public async Task<Pagination<Booking>> GetBookingCustomerLoginPagination(Guid customerId, int pageIndex, int pageSize)
        {
            var itemCount = await _dbSet.Where(x => x.CustomerID == customerId).CountAsync();
            var items = await _dbSet.OrderByDescending(x => x.CreationDate)
                                    .Where(x => x.CustomerID == customerId)
                                    .Skip(pageIndex * pageSize)
                                    .Take(pageSize)
                                    .Include(x => x.Guide)
                                    .Include(x => x.Customer)
                                    .AsNoTracking()
                                    .ToListAsync();

            var result = new Pagination<Booking>()
            {
                PageIndex = pageIndex + 1,
                PageSize = pageSize,
                TotalItemsCount = itemCount,
                Items = items,
            };
            return result;
        }

        public async Task<Pagination<Booking>> GetBookingGuideLoginPagination(Guid guideId, int pageIndex, int pageSize)
        {
            var itemCount = await _dbSet.Where(x => x.GuideID == guideId).CountAsync();
            var items = await _dbSet.OrderByDescending(x => x.CreationDate)
                                    .Where(x => x.GuideID == guideId)
                                    .Skip(pageIndex * pageSize)
                                    .Take(pageSize)
                                    .Include(x => x.Guide)
                                    .Include(x => x.Customer)
                                    .AsNoTracking()
                                    .ToListAsync();

            var result = new Pagination<Booking>()
            {
                PageIndex = pageIndex + 1,
                PageSize = pageSize,
                TotalItemsCount = itemCount,
                Items = items,
            };

            return result;
        }

        public async Task<Booking> GetDetailBookingById(Guid id)
        {
            var result = await _dbContext.Bookings
                .Where(x => x.Id == id)
                .Include(x => x.Guide)
                .Include(x => x.Customer)
                .FirstOrDefaultAsync();
            if (result == null)
            {
                throw new Exception($"Not found any booking with id:{id}");
            }
            return result;
        }

        public async Task<Booking> GetDetailBookingForAdminById(Guid id)
        {
            var result = await _dbContext.Bookings
                 .Where(x => x.Id == id)
                 .Include(x => x.Guide)
                 .Include(x => x.Customer)
                 .Include(x => x.Transactions.Where(x => x.TransactionType != TransactionTypeEnum.TourPayment))
                 .FirstOrDefaultAsync();
            if (result == null)
            {
                throw new Exception($"Not found any booking with id:{id}");
            }
            return result;
        }

        public async Task<int> GetTotalBookingsCompleted() => await _dbSet.CountAsync(x => x.Status == StatusBookingEnum.Completed);

        public Task<int> GetTotalRevenueAsync() => _dbSet.Where(x => x.Status == StatusBookingEnum.Completed)
                                                         .SumAsync(x => x.TotalAmount);

        public Task<List<Booking>> GetAllBookingCompleted() => _dbSet.Where(x => x.Status == StatusBookingEnum.Completed)
                                                                      .ToListAsync();


        public async Task<Pagination<Booking>> ToPaginationWithRelated(int pageIndex, int pageSize)
        {
            var itemCount = await _dbSet.CountAsync();
            var items = await _dbSet.OrderByDescending(x => x.CreationDate)
                                    .Skip(pageIndex * pageSize)
                                    .Take(pageSize)
                                    .Include(x => x.Guide)
                                    .Include(x => x.Customer)
                                    .AsNoTracking()
                                    .ToListAsync();

            var result = new Pagination<Booking>()
            {
                PageIndex = pageIndex,
                PageSize = pageSize,
                TotalItemsCount = itemCount,
                Items = items,
            };

            return result;
        }

        public async Task LoadGuideAndCustomerInformation(Booking booking)
        {
            await _dbContext.Entry(booking)
                            .Reference(x => x.Guide)
                            .LoadAsync();
            if(booking.CustomerID.HasValue)
            {
                await _dbContext.Entry(booking)
                                .Reference(x => x.Customer)
                                .LoadAsync();
            }
        }


    }
}
