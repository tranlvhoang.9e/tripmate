﻿using Application.Interfaces;
using Application.Repositories;
using Application.Utils;
using Domain.Entities;
using Domain.Enums;
using Microsoft.EntityFrameworkCore;

namespace Infrastructures.Repositories
{
    public class UserRepository : GenericRepository<User>, IUserRepository
    {
        private readonly AppDbContext _dbContext;
        private readonly ICurrentTime _timeService;

        public UserRepository(AppDbContext dbContext,
            ICurrentTime timeService,
            IClaimsService claimsService)
            : base(dbContext,
                  timeService,
                  claimsService)
        {
            _dbContext = dbContext;
            _timeService = timeService;
        }

        public Task<bool> CheckEmailNameExisted(string emailName) => _dbContext.Users.AnyAsync(u => u.Email == emailName);

        public Task<bool> CheckPhoneNumberExisted(string phoneNumber) => _dbContext.Users.AnyAsync(u => u.PhoneNumber == phoneNumber);

        public async Task<List<User>> GetTop5GuideByTotalBookings()
        {
            var topCustomer = await _dbSet
            .Where(x => x.Role.Equals(RoleEnum.Customer))
            .OrderByDescending(u => u.TotalBookings)
            .Take(5)
            .AsNoTracking()
            .ToListAsync();

            return topCustomer;
        }

        public Task<int> GetTotalNewCustomerToday() => _dbContext.Users
            .CountAsync(x => x.Role == RoleEnum.Customer && x.CreationDate.Date == _timeService.GetCurrentTime().Date);

        public async Task<User> GetUserByEmailAndPassword(string email, string password)
        {
            var user = await _dbContext.Users
                .FirstOrDefaultAsync(x => x.Email == email);
            if (user == null)
            {
                throw new Exception("Email is not existed!");
            }
            string passwordHash = password.HashPassword(user.Salt);
            if (user.PasswordHash != passwordHash)
            {
                throw new Exception("Password is not correct");
            }
            return user;
        }
    }
}
