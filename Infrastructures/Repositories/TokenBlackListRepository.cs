﻿using Application.Interfaces;
using Application.Repositories;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.Repositories
{
    public class TokenBlackListRepository : ITokenBlackListRepository
    {
        private readonly AppDbContext _dbContext;

        public TokenBlackListRepository(AppDbContext dbContext,
            ICurrentTime timeService,
            IClaimsService claimsService)
        {
            _dbContext = dbContext;
        }

        public async Task AddAsync(TokenBlacklist token) => await _dbContext.TokenBlacklists.AddAsync(token);

        public Task<bool> IsTokenBlacklisted(string token) => _dbContext.TokenBlacklists.AnyAsync(tb => tb.Token == token);
    }
}
