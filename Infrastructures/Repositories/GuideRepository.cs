﻿using Application.Commons;
using Application.Interfaces;
using Application.Repositories;
using Domain.Entities;
using Domain.Enums;
using Microsoft.EntityFrameworkCore;

namespace Infrastructures.Repositories
{
    public class GuideRepository : GenericRepository<User>, IGuideRepository
    {
        private readonly AppDbContext _dbContext;
        public GuideRepository(AppDbContext dbContext,
            ICurrentTime timeService,
            IClaimsService claimsService)
            : base(dbContext,
                  timeService,
                  claimsService)
        {
            _dbContext = dbContext;
        }

        public new Task<List<User>> GetAllAsync() => _dbSet.Where(x => x.Role.Equals(RoleEnum.Guide)).Include(x => x.GuideBookings).OrderByDescending(x => x.CreationDate).ToListAsync();

        public new async Task<Pagination<User>> ToPagination(int pageIndex = 0, int pageSize = 10)
        {
            var itemCount = await _dbSet.CountAsync();
            var items = await _dbSet.Where(x => x.Role.Equals(RoleEnum.Guide))
                                    .Include(x => x.GuideBookings)
                                    .OrderByDescending(x => x.CreationDate)
                                    .Skip(pageIndex * pageSize)
                                    .Take(pageSize)
                                    .AsNoTracking()
                                    .ToListAsync();

            var result = new Pagination<User>()
            {
                PageIndex = pageIndex + 1,
                PageSize = pageSize,
                TotalItemsCount = itemCount,
                Items = items,
            };

            return result;
        }

        public new async Task<User> GetByIdAsync(Guid id)
        {
            var result = await _dbSet.Where(x => x.Role == RoleEnum.Guide)
                .FirstOrDefaultAsync(x => x.Id == id);
            if (result == null)
            {
                throw new Exception($"Not found any guide with id {id}");
            }
            return result;
        }

        public async Task<bool> CheckExistedGuideById(Guid id) => await _dbContext.Users.AnyAsync(u => u.Id == id && u.Role == RoleEnum.Guide);

        public async Task<List<User>> GetTop5GuideByTotalBookings()
        {
            var topGuide = await _dbSet
            .Where(x => x.Role.Equals(RoleEnum.Guide))
            .OrderByDescending(u => u.TotalBookings)
            .Take(5)
            .AsNoTracking()
            .ToListAsync();

            return topGuide;
        }

        public Task<int> GetTotalBookingGuideLoginAsync(Guid guideId) =>  _dbSet.Where(x => x.Role.Equals(RoleEnum.Guide) && x.Id == guideId)
                                                                          .Select(x => (int) x.TotalBookings)
                                                                          .FirstOrDefaultAsync();

        public Task<int> GetTotalHourBookingCurrentGuide(Guid guideId) => _dbContext.Bookings
                                                                          .Where(x => x.GuideID == guideId && x.Status == StatusBookingEnum.Completed)
                                                                          .Select(x => (int) x.TotalHour)
                                                                          .SumAsync();
    }
}
