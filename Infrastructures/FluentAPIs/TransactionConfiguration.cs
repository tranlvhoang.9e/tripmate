﻿using Domain.Entities;
using Domain.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructures.FluentAPIs
{
    public class TransactionConfiguration : IEntityTypeConfiguration<Transaction>
    {
        public void Configure(EntityTypeBuilder<Transaction> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.Property(x => x.Status).HasDefaultValue(StatusTransactionEnum.Pending);
            builder.HasOne(tr => tr.Booking)
                   .WithMany(b => b.Transactions)
                   .HasForeignKey( tr => tr.BookingId);
            builder.HasOne(tr => tr.User)
                   .WithMany(u => u.Transactions)
                   .HasForeignKey(tr => tr.UserId);
        }
    }
}
