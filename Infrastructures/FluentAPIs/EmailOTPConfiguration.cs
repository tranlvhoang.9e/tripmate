﻿using Domain.Entities;
using Domain.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructures.FluentAPIs
{
    public class EmailOTPConfiguration : IEntityTypeConfiguration<EmailOTP>
    {
        public void Configure(EntityTypeBuilder<EmailOTP> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();

            builder.HasOne(e => e.User)
                   .WithOne(e => e.EmailOTP)
                   .HasForeignKey<EmailOTP>(x => x.UserId);
        }
    }
}
