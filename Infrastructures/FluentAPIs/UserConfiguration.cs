﻿using Domain.Entities;
using Domain.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructures.FluentAPIs
{
    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.Property(x => x.FullName).HasMaxLength(100);
            builder.Property(x => x.Email).HasMaxLength(100);
            builder.Property(x => x.PhoneNumber).HasMaxLength(11);
            builder.Property(x => x.Gender).HasMaxLength(15);
            builder.Property(x => x.Languages).HasMaxLength(200);
            builder.Property(x => x.BankName).HasMaxLength(100);
            builder.Property(x => x.BankNumber).HasMaxLength(100);
            builder.Property(x => x.Address).HasMaxLength(200);
            builder.Property(x => x.Status).HasDefaultValue(StatusUserEnum.Active);
            builder.Property(x => x.TotalBookings).HasDefaultValue(0);
        }
    }
}
