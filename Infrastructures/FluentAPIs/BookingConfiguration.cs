﻿using Domain.Entities;
using Domain.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructures.FluentAPIs
{
    public class BookingConfiguration : IEntityTypeConfiguration<Booking>
    {
        public void Configure(EntityTypeBuilder<Booking> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.Property(x => x.AnonymousName).HasMaxLength(100);
            builder.Property(x => x.AnonymousEmail).HasMaxLength(100);
            builder.Property(x => x.AnonymousPhoneNumber).HasMaxLength(11);
            builder.Property(x => x.StartTime).HasMaxLength(5);
            builder.Property(x => x.Status).HasDefaultValue(StatusBookingEnum.Pending);

            builder.HasOne(bk => bk.Customer)
                   .WithMany(u => u.CustomerBookings)
                   .HasForeignKey(bk => bk.CustomerID)
                   .OnDelete(DeleteBehavior.Restrict);
            builder.HasOne(bk => bk.Guide)
                   .WithMany(u => u.GuideBookings)
                   .HasForeignKey(bk => bk.GuideID)
                   .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
