﻿using AutoMapper;
using Application.Commons;
using Domain.Entities;
using Application.ViewModels.UserViewModels;
using Application.ViewModels.GuideViewModels;
using Application.ViewModels.BookingViewModels;
using Application.ViewModels.TransactionViewModels;

namespace Infrastructures.Mappers
{
    public class MapperConfigurationsProfile : Profile
    {
        public MapperConfigurationsProfile()
        {
            CreateMap(typeof(Pagination<>), typeof(Pagination<>));
            CreateMap<UserRegisterModel, User>();
            CreateMap<User, AuthResponseModel>();
            CreateMap<User, UserViewModel>();
            CreateMap<User, CustomerViewTop5Model>();

            #region Map Guide
            CreateMap<User, GuideViewModel>()
                .ForMember(dest => dest.TotalTour, opt => opt.MapFrom(src => src.TotalBookings));
            CreateMap<User, GuideViewDetailModel>()
                .ForMember(dest => dest.TotalTour, opt => opt.MapFrom(src => src.TotalBookings));
            CreateMap<User, BankInformation>();
            CreateMap<User, CustomerViewModel>()
                .ForMember(dest => dest.RegisterDay, opt => opt.MapFrom(src => src.CreationDate))
                .ForMember(dest => dest.TotalBooking, opt => opt.MapFrom(src => src.TotalBookings));
            CreateMap<User, GuideViewForAdminModel>()
                .ForMember(dest => dest.Bookings, opt => opt.MapFrom(src => src.TotalBookings));
            CreateMap<User, GuideViewTop5Model>();
            #endregion

            #region Map Booking
            CreateMap<BookingCustomerCreationModel, Booking>();
            CreateMap<BookingGuestCreationModel, Booking>()
                 .ForMember(dest => dest.AnonymousName, opt => opt.MapFrom(src => src.FullName))
                 .ForMember(dest => dest.AnonymousPhoneNumber, opt => opt.MapFrom(src => src.PhoneNumber))
                 .ForMember(dest => dest.AnonymousEmail, opt => opt.MapFrom(src => src.Email));

            CreateMap<Booking, BookingViewDetailModel>()
                .ForMember(dest => dest.GuideInformation, opt => opt.MapFrom(src => src.Guide))
                .ForMember(dest => dest.UserBookingInformation, opt => opt.MapFrom(src => GetUserBookingInformation(src)));

            CreateMap<Booking, BookingViewDetailForAdminModel>()
                .ForMember(dest => dest.GuideInformation, opt => opt.MapFrom(src => src.Guide))
                .ForMember(dest => dest.UserBookingInformation, opt => opt.MapFrom(src => GetUserBookingInformation(src)))
                .ForMember(dest => dest.Transaction, opt => opt.MapFrom(src => src.Transactions.FirstOrDefault()));

            CreateMap<User, UserBasicInformationModel>();
            CreateMap<User, GuideBasicInformationModel>();

            CreateMap<Booking, BookingViewModel>()
                .ForMember(dest => dest.CustomerID, opt => opt.MapFrom(src => src.CustomerID.ToString()))
                .ForMember(dest => dest.CustomerName, opt => opt.MapFrom(src => src.Customer != null ? src.Customer.FullName : src.AnonymousName))
                .ForMember(dest => dest.GuideID, opt => opt.MapFrom(src => src.GuideID.ToString()))
                .ForMember(dest => dest.GuideName, opt => opt.MapFrom(src => src.Guide.FullName));

            CreateMap<Booking, BookingCustomerViewModel>()
                .ForMember(dest => dest.GuideID, opt => opt.MapFrom(src => src.GuideID.ToString()))
                .ForMember(dest => dest.GuideName, opt => opt.MapFrom(src => src.Guide.FullName));

            CreateMap<Booking, BookingGuideViewModel>()
                .ForMember(dest => dest.CustomerID, opt => opt.MapFrom(src => src.GuideID.ToString()))
                .ForMember(dest => dest.CustomerName, opt => opt.MapFrom(src => src.Customer != null ? src.Customer.FullName : src.AnonymousName));
            #endregion

            #region Map Transaction
            CreateMap<Transaction, TransactionViewModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id.ToString()))
                .ForMember(dest => dest.Sender, opt => opt.MapFrom(src =>
                            src.From != "Tripmate" ? (src.UserId.HasValue ? src.User.FullName : src.Booking.AnonymousName) : src.From))
                .ForMember(dest => dest.Receiver, opt => opt.MapFrom(src =>
                            src.To != "Tripmate" ? src.User.FullName : src.To))
                .ForMember(dest => dest.CreationDate, opt => opt.MapFrom(src => src.CreationDate))
                .ForMember(dest => dest.BookingStatus, opt => opt.MapFrom(src => src.Booking.Status));
            #endregion
        }

        private static UserBasicInformationModel GetUserBookingInformation(Booking booking)
        {
            if (booking.CustomerID == null)
            {
                return new UserBasicInformationModel
                {
                    FullName = booking.AnonymousName,
                    Email = booking.AnonymousEmail,
                    PhoneNumber = booking.AnonymousPhoneNumber
                };
            }

            return new UserBasicInformationModel
            {
                Id = booking.CustomerID.ToString(),
                FullName = booking.Customer.FullName,
                Email = booking.Customer.Email,
                PhoneNumber = booking.Customer.PhoneNumber
            };
        }
    }
}
