﻿namespace Domain.Enums
{
    public enum StatusUserEnum
    {
        Inactive,
        Active,
        Banned
    }
}
