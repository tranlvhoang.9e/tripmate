﻿namespace Domain.Enums
{
    public enum TransactionTypeEnum
    {
        PlatformFee,
        TourPayment,
        GuideFee,
    }
}
