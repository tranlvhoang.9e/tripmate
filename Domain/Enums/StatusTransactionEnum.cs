﻿namespace Domain.Enums
{
    public enum StatusTransactionEnum
    {
        Pending,
        PendingConfirmed,
        Completed,
        Refund,
        Failed
    }
}
