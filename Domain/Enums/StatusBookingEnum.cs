﻿namespace Domain.Enums
{
    public enum StatusBookingEnum
    {
        Pending, 
        Confirmed,
        Cancelled,
        Completed
    }
}
