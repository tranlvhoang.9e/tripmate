﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class EmailOTP
    {
        public uint Id { get; set; }
        public Guid UserId { get; set; }
        public User User { get; set; }
        public string OTP { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime ExpiredAt { get; set; }
    }
}
