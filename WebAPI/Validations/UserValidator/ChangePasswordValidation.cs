﻿using Application.ViewModels.UserViewModels;
using FluentValidation;

namespace WebAPI.Validations.UserValidator
{
    public class ChangePasswordValidation : AbstractValidator<ChangePasswordModel>
    {
        public ChangePasswordValidation()
        {
            RuleFor(x => x.OldPassword).NotEmpty();
            RuleFor(x => x.NewPassword).NotEmpty().MinimumLength(6)
                .WithMessage("Password must be at least 6 characters long!")
            .Matches(@"[0-9]+")
                .WithMessage("Your password must contain at least one number!");
            RuleFor(x => x.NewPassword).Equal(x => x.ConfirmNewPassword)
                .WithMessage("Your password confirmed is wrong!");
        }
    }
}
