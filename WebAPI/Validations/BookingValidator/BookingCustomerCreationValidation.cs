﻿using Application.ViewModels.BookingViewModels;
using FluentValidation;

namespace WebAPI.Validations.BookingValidator
{
    public class BookingCustomerCreationValidation : AbstractValidator<BookingCustomerCreationModel>
    {
        public BookingCustomerCreationValidation()
        {
            RuleFor(x => x.BookingDate).NotEmpty()
                .Must(BeAValidDate).WithMessage("Date is invalid, format: yyyy-mm-dd");
            RuleFor(x => x.TotalHour).NotEmpty();
            RuleFor(x => x.Location).NotEmpty();
            RuleFor(x => x.StartTime).NotEmpty().WithMessage("Time is required.")
                .Matches(@"^((?:[01]\d|2[0-3]):[0-5]\d|24:00)$")
                .WithMessage("Time must be in valid format, e.g., 00:00 to 24:00")
                .Must((booking, timeString) => BeAValidTime(booking.BookingDate, timeString))
                .WithMessage("Time is invalid");
        }

        private bool BeAValidTime(DateTime bookingDate, string timeString)
        {
            TimeSpan inputTime;
            if (TimeSpan.TryParse(timeString, out inputTime))
            {
                if (bookingDate.Date > DateTime.Now.Date)
                {
                    return true;
                }
                TimeSpan currentTime = DateTime.Now.TimeOfDay;
                if (inputTime >= currentTime)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return false;
        }

        private bool BeAValidDate(DateTime date)
        {
            return !date.Equals(default(DateTime)) && date >= DateTime.Now.Date;
        }
    }
}
