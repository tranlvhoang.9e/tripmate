﻿using Application.Interfaces;
using Domain.Enums;
using System.Security.Claims;

namespace WebAPI.Services
{
    public class ClaimsService : IClaimsService
    {
        public ClaimsService(IHttpContextAccessor httpContextAccessor)
        {
            // todo implementation to get the current userId
            var Id = httpContextAccessor.HttpContext?.User?.FindFirstValue("userId");
            GetCurrentUserId = string.IsNullOrEmpty(Id) ? Guid.Empty : Guid.Parse(Id);

            var role = httpContextAccessor.HttpContext?.User?.FindFirstValue(ClaimTypes.Role);
            GetCurrentUserRole = string.IsNullOrEmpty(role) ? null : (RoleEnum)Enum.Parse(typeof(RoleEnum), role);

            var authorizationHeader = httpContextAccessor.HttpContext?.Request.Headers["Authorization"];
            GetToken = authorizationHeader.HasValue ? authorizationHeader.Value.ToString().Replace("Bearer ", "") : string.Empty;
        }

        public Guid GetCurrentUserId { get; }

        public RoleEnum? GetCurrentUserRole { get; }

        public string GetToken { get; set; }
    }
}
