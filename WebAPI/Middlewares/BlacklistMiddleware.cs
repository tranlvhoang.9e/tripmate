﻿using Application;
using Application.Interfaces;
using static System.Net.Mime.MediaTypeNames;

namespace WebAPI.Middlewares
{
    public class BlacklistMiddleware : IMiddleware
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IClaimsService _claimService;

        public BlacklistMiddleware(IUnitOfWork unitOfWork, IClaimsService claimService)
        {
            _unitOfWork = unitOfWork;
            _claimService = claimService;
        }

        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            var token = _claimService.GetToken;

            if (!string.IsNullOrEmpty(token) && await _unitOfWork.TokenBlackListRepository.IsTokenBlacklisted(token))
            {
                context.Response.StatusCode = StatusCodes.Status401Unauthorized;
                await context.Response.WriteAsync("Token is blacklisted");
                return;
            }
            await next(context);
        }
    }
}
