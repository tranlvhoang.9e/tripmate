﻿using Application.Commons;
using Application.Interfaces;
using Application.ViewModels.UserViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api")]
    [ApiController]
    public class AuthController : BaseController
    {
        private readonly IUserService _userService;
        public AuthController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost("users/register")]
        [AllowAnonymous]
        public async Task<ApiResponse<string>> RegisterAsync([FromBody] UserRegisterModel registerObject) => await _userService.RegisterUserAsync(registerObject);

        [HttpPost("auth/login")]
        [AllowAnonymous]
        public async Task<ApiResponse<AuthResponseModel>> LoginAsync([FromBody] UserLoginModel loginObject) => await _userService.LoginAsync(loginObject);

        [HttpPost("auth/logout")]
        [Authorize]
        public async Task<ApiResponse<string>> LogoutAsync() => await _userService.LogoutAsync();

    }
}
