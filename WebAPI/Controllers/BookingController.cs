﻿using Application.Commons;
using Application.Interfaces;
using Application.ViewModels.BookingViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/v1")]
    [ApiController]
    public class BookingController : Controller
    {
        private readonly IBookingService _bookingService;

        public BookingController(IBookingService bookingService)
        {
            _bookingService = bookingService;
        }

        [HttpPost("bookings-customer")]
        [Authorize(Roles = AppRole.CUSTOMER)]
        public async Task<ApiResponse<BookingViewDetailModel>> CreateBookingCustomerAsync([FromBody] BookingCustomerCreationModel objectBookingCustomer) => await _bookingService.CreateBookingCustomerAsync(objectBookingCustomer);

        [HttpPost("bookings-guest")]
        [AllowAnonymous]
        public async Task<ApiResponse<BookingViewDetailModel>> CreateBookingGuestAsync([FromBody] BookingGuestCreationModel objectBookingGuest) => await _bookingService.CreateBookingGuestAsync(objectBookingGuest);

        [HttpGet("bookings")]
        [Authorize(Roles = AppRole.ADMIN)]
        public async Task<ApiResponse<List<BookingViewModel>>> GetAllBookingAsync() => await _bookingService.GetAllBookingAsync();

        [HttpGet("bookings-filter")]
        [Authorize(Roles = AppRole.ADMIN)]
        public async Task<ApiResponse<Pagination<BookingViewModel>>> GetBookingFilterAsync([FromQuery] BookingFilterModel bookingFilterModel) => await _bookingService.GetBookingFilterAsync(bookingFilterModel);

        [HttpGet("bookings-pagination")]
        [Authorize(Roles = AppRole.ADMIN)]
        public async Task<ApiResponse<Pagination<BookingViewModel>>> GetBookingPaginationAsync(int pageNumber = 1, int pageSize = 10) => await _bookingService.GetBookingPaginationAsync(pageNumber, pageSize);

        [HttpGet("bookings-customer")]
        [Authorize(Roles = AppRole.CUSTOMER)]
        public async Task<ApiResponse<List<BookingCustomerViewModel>>> GetAllBookingCustomerLoginAsync() => await _bookingService.GetAllBookingCustomerLoginAsync();

        [HttpGet("bookings-customer-pagination")]
        [Authorize(Roles = AppRole.CUSTOMER)]
        public async Task<ApiResponse<Pagination<BookingCustomerViewModel>>> GetBookingCustomerLoginPaginationAsync(int pageNumber = 1, int pageSize = 10) => await _bookingService.GetBookingCustomerLoginPaginationAsync(pageNumber, pageSize);

        [HttpGet("bookings-customer-filter")]
        [Authorize(Roles = AppRole.CUSTOMER)]
        public async Task<ApiResponse<Pagination<BookingCustomerViewModel>>> GetBookingCustomerFilterAsync([FromQuery] BookingFilterModel bookingFilterModel) => await _bookingService.GetBookingCustomerFilterAsync(bookingFilterModel);

        [HttpGet("bookings-guide")]
        [Authorize(Roles = AppRole.GUIDE)]
        public async Task<ApiResponse<List<BookingGuideViewModel>>> GetAllBookingGuideLoginAsync() => await _bookingService.GetAllBookingGuideLoginAsync();

        [HttpGet("bookings-guide-pagination")]
        [Authorize(Roles = AppRole.GUIDE)]
        public async Task<ApiResponse<Pagination<BookingGuideViewModel>>> GetBookingGuideLoginPaginationAsync(int pageNumber = 1, int pageSize = 10) => await _bookingService.GetBookingGuideLoginPaginationAsync(pageNumber, pageSize);

        [HttpGet("bookings-guide-filter")]
        [Authorize(Roles = AppRole.GUIDE)]
        public async Task<ApiResponse<Pagination<BookingGuideViewModel>>> GetBookingGuideFilterAsync([FromQuery] BookingFilterModel bookingFilterModel) => await _bookingService.GetBookingGuideFilterAsync(bookingFilterModel);

        [HttpGet("bookings/{id}")]
        [Authorize]
        public async Task<ApiResponse<BookingViewDetailModel>> GetBookingDetailAsync(string id) => await _bookingService.GetBookingDetailAsync(id);

        [HttpPut("bookings/{id}/status")]
        [Authorize]
        public async Task<ApiResponse<string>> UpdateStatusBookingAsync(string id, int status) => await _bookingService.UpdateStatusBookingAsync(id, status);
    }
}
