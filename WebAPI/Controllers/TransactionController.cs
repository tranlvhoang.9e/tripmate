﻿using Application.Commons;
using Application.Interfaces;
using Application.ViewModels.TransactionViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/v1")]
    [ApiController]
    public class TransactionController : Controller
    {
        private readonly ITransactionService _transactionService;

        public TransactionController(ITransactionService transactionService)
        {
            _transactionService = transactionService;
        }

        [HttpGet("transactions")]
        [Authorize(Roles = AppRole.ADMIN)]
        public async Task<ApiResponse<List<TransactionViewModel>>> GetAllTransactionAsync() => await _transactionService.GetAllTransactionAsync();

        [HttpGet("transactions-filter")]
        [Authorize(Roles = AppRole.ADMIN)]
        public async Task<ApiResponse<Pagination<TransactionViewModel>>> FilterTransactionAsync([FromQuery] TransactionFilterModel transactionFilterModel) => await _transactionService.FilterTransactionAsync(transactionFilterModel);

        [HttpGet("transactions-guide")]
        [Authorize(Roles = AppRole.GUIDE)]
        public async Task<ApiResponse<List<TransactionViewModel>>> GetAllTransactionGuideAsync() => await _transactionService.GetAllTransactionGuideAsync();

        [HttpGet("transactions-guide-filter")]
        [Authorize(Roles = AppRole.GUIDE)]
        public async Task<ApiResponse<Pagination<TransactionViewModel>>> FilterTransactionGuideAsync([FromQuery] TransactionFilterModel transactionFilterModel) => await _transactionService.FilterTransactionGuideAsync(transactionFilterModel);

        [HttpPut("transactions/{id}/status")]
        [Authorize(Roles = AppRole.ADMIN)]
        public async Task<ApiResponse<string>> UpdateStatusTransactionAsync(string id, int status) => await _transactionService.UpdateStatusTransactionAsync(id, status);

        [HttpPut("transactions/{id}/confirm")]
        [Authorize(Roles = AppRole.GUIDE)]
        public async Task<ApiResponse<string>> ConfirmPaymentTransactionGuideAsync(string id) => await _transactionService.ConfirmPaymentTransactionGuideAsync(id);
    }
}
