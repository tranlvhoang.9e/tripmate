﻿using Application.Commons;
using Application.Interfaces;
using Application.ViewModels.GuideViewModels;
using FluentValidation.Validators;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing.Constraints;
using System.ComponentModel.DataAnnotations;

namespace WebAPI.Controllers
{
    [Route("api/v1")]
    [ApiController]
    public class GuideController : ControllerBase
    {
        private readonly IGuideService _guideService;

        public GuideController(IGuideService guideService)
        {
            _guideService = guideService;
        }

        [HttpGet("guides")]
        [AllowAnonymous]
        public async Task<ApiResponse<List<GuideViewModel>>> GetAllGuidesAsync() => await _guideService.GetAllGuidesAsync();

        [HttpGet("guides-pagination")]
        [AllowAnonymous]
        public async Task<ApiResponse<Pagination<GuideViewModel>>> GetGuidesPaginationAsync(int pageNumber = 1, int pageSize = 10) => await _guideService.GetGuidesPaginationAsync(pageNumber, pageSize);

        [HttpGet("guides/{id}")]
        [AllowAnonymous]
        public async Task<ApiResponse<GuideViewDetailModel>> GetDetailGuidAsync(string id) => await _guideService.GetDetailGuidAsync(id);

        [HttpGet("guides/bank-information/{id}")]
        [AllowAnonymous]
        public async Task<ApiResponse<BankInformation>> GetBankInformationGuide(string id) => await _guideService.GetBankInformationGuide(id);

        [HttpGet("guides/me")]
        [Authorize(Roles = AppRole.GUIDE)]
        public async Task<ApiResponse<GuideViewDetailModel>> GetCurrentGuideAsync() => await _guideService.GetCurrentGuideAsync();

        [HttpPut("guides/me")]
        [Authorize(Roles = AppRole.GUIDE)]
        public async Task<ApiResponse<string>> UpdateGuideAsync([FromBody]UpdateGuideModel updateGuideModel) => await _guideService.UpdateGuideInformationAsync(updateGuideModel);

        [HttpPut("guides/me/status")]
        [Authorize(Roles = AppRole.GUIDE)]
        public async Task<ApiResponse<string>> UpdateStatusGuideAsync() => await _guideService.UpdateStatusGuideAsync();

        [HttpGet("guides/me/totalbooking")]
        [Authorize(Roles = AppRole.GUIDE)]
        public async Task<ApiResponse<int>> GetTotalBookingGuideLoginAsync() => await _guideService.GetTotalBookingGuideLoginAsync();

        [HttpGet("guides/me/totalhour")]
        [Authorize(Roles = AppRole.GUIDE)]
        public async Task<ApiResponse<int>> GetTotalHourGuideLoginAsync() => await _guideService.GetTotalHourBookingGuideLoginAsync();
    }
}
