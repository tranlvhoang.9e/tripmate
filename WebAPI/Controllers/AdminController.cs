﻿using Application.Commons;
using Application.Interfaces;
using Application.Services;
using Application.ViewModels.BookingViewModels;
using Application.ViewModels.GuideViewModels;
using Application.ViewModels.StatisticViewModels;
using Application.ViewModels.UserViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Data;

namespace WebAPI.Controllers
{
    [Route("api/v1")]
    [ApiController]
    public class AdminController : Controller
    {
        private readonly IUserService _userService;
        private readonly IGuideService _guideService;
        private readonly IBookingService _bookingService;
        private readonly ITransactionService _transactionService;

        public AdminController(IUserService userService, IGuideService guideService, IBookingService bookingService, ITransactionService transactionService)
        {
            _userService = userService;
            _guideService = guideService;
            _bookingService = bookingService;
            _transactionService = transactionService;
        }

        [HttpGet("admin/customers")]
        [Authorize(Roles = AppRole.ADMIN)]
        public async Task<ApiResponse<Pagination<CustomerViewModel>>> FiltelAllCustomerAsync([FromQuery] CustomerFilterModel customerFilterModel) => await _userService.FiltelCustomerAsync(customerFilterModel);

        [HttpPut("admin/customers/{id}/ban")]
        [Authorize(Roles = AppRole.ADMIN)]
        public async Task<ApiResponse<string>> BanCustomerAsync(string id) => await _userService.BanCustomerAsync(id);

        [HttpGet("admin/guides")]
        [Authorize(Roles = AppRole.ADMIN)]
        public async Task<ApiResponse<Pagination<GuideViewForAdminModel>>> GetFilteredAdminGuides([FromQuery] GuideFilterModel guideFilterModel) => await _guideService.GetFilteredAdminGuides(guideFilterModel);

        [HttpPut("admin/guides/{id}/ban")]
        [Authorize(Roles = AppRole.ADMIN)]
        public async Task<ApiResponse<string>> BanGuideAsync(string id) => await _guideService.BanGuideAsync(id);

        [HttpGet("admin/bookings/{id}")]
        [Authorize(Roles = AppRole.ADMIN)]
        public async Task<ApiResponse<BookingViewDetailForAdminModel>> GetBookingDetailForAdminAsync(string id) => await _bookingService.GetBookingDetailForAdminAsync(id);

        [HttpGet("admin/guides/top")]
        [Authorize(Roles = AppRole.ADMIN)]
        public async Task<ApiResponse<List<GuideViewTop5Model>>> GetTop5GuideAsync() => await _guideService.GetTop5GuideAsync();

        [HttpGet("admin/customers/top")]
        [Authorize(Roles = AppRole.ADMIN)]
        public async Task<ApiResponse<List<CustomerViewTop5Model>>> GetTop5CustomerAsync() => await _userService.GetTop5CustomerAsync();

        [HttpGet("admin/bookings/total")]
        [Authorize(Roles = AppRole.ADMIN)]
        public async Task<ApiResponse<int>> GetTotalBookingAsync() => await _bookingService.GetTotalBookingCompletedAsync();

        [HttpGet("admin/bookings/revenue")]
        [Authorize(Roles = AppRole.ADMIN)]
        public async Task<ApiResponse<int>> GetTotalRevenueAsync() => await _bookingService.GetTotalRevenueAsync();

        [HttpGet("admin/customers/new-today")]
        [Authorize(Roles = AppRole.ADMIN)]
        public async Task<ApiResponse<int>> GetTotalNewCustomerTodayAsync() => await _userService.GetTotalNewCustomerTodayAsync();

        [HttpGet("admin/bookings/earning")]
        [Authorize(Roles = AppRole.ADMIN)]
        public async Task<ApiResponse<int>> GetTotalEarningAsync() => await _transactionService.GetTotalEarningAsync();

        [HttpGet("admin/statistics/tourduration")]
        [Authorize(Roles = AppRole.ADMIN)]
        public async Task<ApiResponse<List<StatisticTourDurationViewModel>>> GetTourDurationStatisticsAsync() => await _bookingService.GetTourDurationStatisticsAsync();

    }
}
