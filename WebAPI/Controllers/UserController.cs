﻿using Application.Commons;
using Application.Interfaces;
using Application.ViewModels.UserViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/v1")]
    [ApiController]
    public class UserController : Controller
    {
        private readonly IUserService _userService;
        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPut("users/password")]
        [Authorize]
        public async Task<ApiResponse<string>> ChangePasswordAsync([FromBody] ChangePasswordModel changePasswordModel) => await _userService.ChangePasswordAsync(changePasswordModel);

        [HttpGet("users/me")]
        [Authorize(Roles = AppRole.ADMIN + ", " + AppRole.CUSTOMER)]
        public async Task<ApiResponse<UserViewModel>> GetCurrentUserAsync() => await _userService.GetCurrentUserAsync();
    }
}
